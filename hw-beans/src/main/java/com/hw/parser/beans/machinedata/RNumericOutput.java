/**
 * 
 */
package com.hw.parser.beans.machinedata;

import javax.annotation.ManagedBean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The numeric outputs data process image bean.
 * <p>
 * This bean contains an {@code int} that represents the enum of the current
 * numeric output and 2 {@code float} data types.
 * 
 * @author kaja
 * @version 1.0
 */
@Data
@Builder @NoArgsConstructor @AllArgsConstructor
@ManagedBean
public class RNumericOutput {

	private int numericOutput;
	
	private float dem;
	
	private float cd;

}
