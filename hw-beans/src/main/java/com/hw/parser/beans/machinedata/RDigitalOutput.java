/**
 * 
 */
package com.hw.parser.beans.machinedata;

import javax.annotation.ManagedBean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The digital outputs data process image bean.
 * <p>
 * This bean contains an {@code int} that represents the enum of the current
 * digital output and 2 {@code boolean} data types.
 * 
 * @author kaja
 * @version 1.0
 */
@Data
@Builder @NoArgsConstructor @AllArgsConstructor
@ManagedBean
public class RDigitalOutput {

	private int digitalOutputs;
	
	private boolean hmi;
	
	private boolean dem;

}
