/**
 * 
 */
package com.hw.parser.beans.machinedata;

import javax.annotation.ManagedBean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The numeric inputs data process image bean.
 * <p>
 * This bean contains an {@code int} that represents the enum of the current
 * numeric input and 3 {@code float} data types.
 * 
 * @author kaja
 * @version 1.0
 */
@Data
@Builder @NoArgsConstructor @AllArgsConstructor
@ManagedBean
public class RNumericInput {

	private int numericInput;
	
	private float r;
	
	private float f;
	
	private float cd;

}
