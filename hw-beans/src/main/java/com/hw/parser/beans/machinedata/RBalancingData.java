/**
 * 
 */
package com.hw.parser.beans.machinedata;

import javax.annotation.ManagedBean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The balancing data process image bean.
 * <p>
 * This bean contains 13 {@code float} data types.
 * 
 * @author kaja
 * @version 1.0
 */
@Data
@Builder @NoArgsConstructor @AllArgsConstructor
@ManagedBean
public class RBalancingData {
	
	private float ufHost;
	
	private float ufControl;
	
	private float ufWeight;
	
	private float acidFlowConrol;
	
	private float acidWeight;
	
	private float acidDensity;
	
	private float baseFlowControl;
	
	private float baseWeight;
	
	private float baseDensity;
	
	private float scale1row;
	
	private float scale2row;
	
	private float scaleDeviation;
	
	private float scaleSumCurrent;
	
	private float balanceOffset;

}
