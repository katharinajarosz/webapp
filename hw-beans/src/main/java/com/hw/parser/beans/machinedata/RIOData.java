/**
 * 
 */
package com.hw.parser.beans.machinedata;

import javax.annotation.ManagedBean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The input / output data process image bean.
 * <p>
 * This bean contains 2 labview timestamps as {@code long} and 4 {@code byte}
 * data types.
 * 
 * @author kaja
 * @version 1.0
 */
@Data
@Builder @NoArgsConstructor @AllArgsConstructor
@ManagedBean
public class RIOData {

	
	private long timestamp1;
	
	private long timestamp2;
	
	private byte supplyMode;
	
	private byte wasteMode;
	
	private byte gpMode;
	
	private byte heaterMode;

}
