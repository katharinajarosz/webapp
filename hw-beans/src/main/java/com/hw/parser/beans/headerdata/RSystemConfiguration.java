/**
 * 
 */
package com.hw.parser.beans.headerdata;

import javax.annotation.ManagedBean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The system configuration process image bean.
 * <p>
 * This bean contains a section size as {@code int}, a machine number as
 * {@code String}, a host version as {@code String}, a control version as
 * {@code String}, a protect version as {@code String}, a parameter file as
 * {@code String} and the clinic name as {@code String}.
 * 
 * @author kaja
 * @version 1.0
 */
@Data
@Builder @NoArgsConstructor @AllArgsConstructor
@ManagedBean
public class RSystemConfiguration {

	
	private int sectionSize;
	
	private String machineNumber;
	
	private String hostVersion;
	
	private String controlVersion;
	
	private String protectVersion;
	
	private String parameterFile;
	
	private String clinicName;

}
