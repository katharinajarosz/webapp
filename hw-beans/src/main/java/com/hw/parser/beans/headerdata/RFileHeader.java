/**
 * 
 */
package com.hw.parser.beans.headerdata;

import java.math.BigDecimal;

import javax.annotation.ManagedBean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The file header process image bean.
 * <p>
 * This bean contains a file identifier as {@code String}, a section size as
 * {@code int}, an offset to machine data as {@code int} and file format version
 * as {@code BigDecimal}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Data
@Builder @NoArgsConstructor @AllArgsConstructor
@ManagedBean
public class RFileHeader {

	
	private String fileIdentifier;
	
	private int sectionSize;
	
	private int offset;
	
	private BigDecimal fileFormatVersion;

}
