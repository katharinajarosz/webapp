/**
 * 
 */
package com.hw.parser.beans.machinedata;

import javax.annotation.ManagedBean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * The analog in data process image bean.
 * <p>
 * This bean contains two {@code float} data types.
 * 
 * @author kaja
 * @version 1.0
 *
 */

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ManagedBean
public class RAnalogIn {

	private float f;
	
	private float cd;

}
