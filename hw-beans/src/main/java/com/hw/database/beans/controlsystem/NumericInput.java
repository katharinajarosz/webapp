package com.hw.database.beans.controlsystem;

import java.sql.Timestamp;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.hw.database.beans.controlsystem.types.NumericInputType;
import com.hw.database.beans.globaldata.MachineRun;
import com.hw.parser.beans.machinedata.RNumericInput;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The numeric input bean.
 * <p>
 * Contain an unidirectional relationship to {@link MachineRun} 
 * and an unidirectional relationship to {@link NumericInputType}.
 * 
 * @author kaja
 * @version 1.0
 * @see RNumericInput
 *
 */
@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor
@Entity
@Table(name = "NumericInput", schema = "machine")
@ManagedBean
public class NumericInput {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "NumericInputId")
	private long id;
	
	@Column(name = "R")
	private float r;
	
	@Column(name = "F")
	private float f;
	
	@Column(name = "CD")
	private float cd;
	
	@ManyToOne
	@JoinColumn(name = "Machine_Run_ID")
	private MachineRun machineRun;
	
	@Column(name = "Machine_Data_Loop_Timestamp")
	private Timestamp machineDataLoopTimestamp;
	
	@ManyToOne
	@JoinColumn(name = "Numeric_Input_Type_ID")
	private NumericInputType type;
	


}
