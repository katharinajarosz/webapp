package com.hw.database.beans.protectivesystem;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.hw.database.beans.protectivesystem.PumpData.PumpDataType;

/**
 * Converter for {@link PumpData}.
 * <p>
 * Specifies the conversion of {@link PumpDataType} to {@link String} and vice versa.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Converter
public class PumpDataConverter implements AttributeConverter<PumpDataType, String> {

	@Override
	public String convertToDatabaseColumn(PumpDataType attribute) {
		if (attribute == null) {
			return null;
		}
		return attribute.getType();
	}

	@Override
	public PumpDataType convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return null;
		}
		return PumpDataType.fromType(dbData);
	}
}