package com.hw.database.beans.controlsystem;

import java.sql.Timestamp;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.hw.database.beans.controlsystem.types.DigitalOutputType;
import com.hw.database.beans.globaldata.MachineRun;
import com.hw.parser.beans.machinedata.RDigitalOutput;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The digital output bean.
 * <p>
 * Contain an unidirectional relationship to {@link MachineRun} and an unidirectional 
 * relationship to {@link DigitalOutputType}.
 * 
 * @author kaja
 * @version 1.0
 * @see RDigitalOutput
 *
 */
@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor
@Entity
@Table(name = "DigitalOutput", schema = "machine")
@ManagedBean
public class DigitalOutput {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "DigitalOutputId")
	private long id;
	
	@Column(name = "HMI")
	private boolean hmi;
	
	@Column(name = "DEM")
	private boolean dem;
		
	@ManyToOne
	@JoinColumn(name = "Machine_Run_ID")
	private MachineRun machineRun;
	
	@Column(name = "Machine_Data_Loop_Timestamp")
	private Timestamp machineDataLoopTimestamp;
	
	@ManyToOne
	@JoinColumn(name = "Digital_Output_Type_ID")
	private DigitalOutputType type;

}
