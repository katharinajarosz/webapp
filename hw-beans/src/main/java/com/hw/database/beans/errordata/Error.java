package com.hw.database.beans.errordata;

import java.util.List;

import javax.annotation.ManagedBean;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The error bean.
 * <p>
 * Contains an unidirectional relationship to
 * {@link Severity} and a bidirectional relationship to
 * {@link Catchphrase}
 * 
 * @author kaja
 * @version 2.0
 *
 */
@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor
@Entity
@Table(name = "Error", schema = "error")
@ManagedBean
public class Error {
	
	@Id
	@Column(name="ErrorId")
	private int Id;
	
	@Column(name="ErrorCode")
	private String code;
	
	@Column(name="State")
	private String state;
	
	@Column(name="Condition")
	private String condition;
	
	@Column(name="Escalation")
	private int escalation;
	
	@Column(name="Deprecated")
	private boolean deprecated;
	
	@Column(name="Comment")
	private String comment;
	
	@Column(name="Abortion")
	private boolean abortion;
	
	@Column(name="Reviewer")
	private String reviewer;
	
	@Column(name="Reviewed")
	private String reviewed;
	
	@Column(name="Acknowledge")
	private boolean acknowledge;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "SeverityId")
	private Severity severity;
	
	@OneToMany(mappedBy = "error", 
			cascade = CascadeType.ALL,
	        orphanRemoval = true)
	private List<ErrorCatchphrase> catchphrases;
	

}
