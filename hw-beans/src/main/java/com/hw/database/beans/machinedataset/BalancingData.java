/**
 * 
 */
package com.hw.database.beans.machinedataset;

import java.sql.Timestamp;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import com.hw.database.beans.globaldata.MachineRun;
import com.hw.parser.beans.machinedata.RBalancingData;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The balancing data bean.
 * <p>
 * Contains an unidirectional relationship to {@link MachineRun}.
 * 
 * @author kaja
 * @version 1.0
 * @see RBalancingData
 *
 */
@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor
@Entity
@Table(name = "BalancingData", schema = "machine")
@ManagedBean
public class BalancingData {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "BalancingDataId")
	private long id;

	@Column
	private float ufHost;
	
	@Column
	private float ufControl;
	
	@Column
	private float ufWeight;
	
	@Column
	private float acidFlowConrol;
	
	@Column
	private float acidWeight;
	
	@Column
	private float baseFlowControl;
	
	@Column
	private float baseWeight;
	
	@Column
	private float scale1row;
	
	@Column
	private float scale2row;
	
	@Column
	private float scaleDeviation;
	
	@Column
	private float scaleSumCurrent;
	
	@Column
	private float balanceOffset;
	
	@ManyToOne
	@JoinColumn(name = "Machine_Run_ID")
	private MachineRun machineRun;
	
	@Column(name = "Machine_Data_Loop_Timestamp")
	private Timestamp machineDataLoopTimestamp;

}
