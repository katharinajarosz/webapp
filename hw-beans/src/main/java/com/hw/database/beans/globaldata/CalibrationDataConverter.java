package com.hw.database.beans.globaldata;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.hw.database.beans.globaldata.CalibrationData.CalibrationType;

/**
 * Converter for {@link CalibrationData}.
 * <p>
 * Specifies the conversion of {@link CalibrationType} to {@link String} and vice versa.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Converter
public class CalibrationDataConverter implements AttributeConverter<CalibrationType, String> {

	@Override
	public String convertToDatabaseColumn(CalibrationType attribute) {
		if (attribute == null) {
			return null;
		}
		return attribute.getType();
	}

	@Override
	public CalibrationType convertToEntityAttribute(String dbData) {
		if (dbData == null) {
			return null;
		}
		return CalibrationType.fromType(dbData);
	}
}