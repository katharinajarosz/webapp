package com.hw.database.beans.errordata;

import java.sql.Timestamp;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.hw.database.beans.globaldata.MachineRun;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * An active error bean.
 * <p>
 * Contains a bidirectional relationship to {@link MachineRun} and unidirectional relationships to {@link Error} 
 * and {@link Severity}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor 
@Entity
@Table(name = "ActiveError", schema = "machine")
@ManagedBean
public class ActiveError {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="ActiveErrorId")
	private long id;
	
	@Column(name = "Start")
	private Timestamp start;
	
	@Column(name = "Ende")
	private Timestamp end;
	
	@ManyToOne
	@JoinColumn(name = "SeverityId")
	private Severity severity;
	
	@ManyToOne
	@JoinColumn(name = "ErrorId")
	private Error error;
	
	@ManyToOne(optional = true, fetch = FetchType.LAZY)
	@JoinColumn(name = "Machine_Run_ID")
	private MachineRun machineRun;
	
}
