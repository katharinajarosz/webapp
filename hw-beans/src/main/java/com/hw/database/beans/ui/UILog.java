package com.hw.database.beans.ui;

import java.sql.Timestamp;

import javax.annotation.ManagedBean;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.hw.database.beans.globaldata.MachineRun;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 
 * The user interface log bean.
 * <p>
 * Contain unidirectional relationships to {@link Component},
 * {@link Event}, {@link Identifier} and {@link Value}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Getter @Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "UILog", schema = "ui")
@ManagedBean
public class UILog {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "UILogId")
	private long ID;
	
	@Column(name = "Timestamp")
	private Timestamp timestamp;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "Machine_Run_ID")
	private MachineRun machineRun;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ComponentId")
	private Component component;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "IdentifierId")
	private Identifier identufier;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "EventId")
	private Event event;
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "ValueId")
	private Value value;
	 

}
