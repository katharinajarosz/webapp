package com.hw.database.beans.globaldata;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/** The internationalization bean.
 * 
 * @author kaja
 *
 */
@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor 
@Entity
@Table(name = "Internationalization")
@ManagedBean
public class Internationalization {
	
	@Id
	@Column(name="IsoId")
	private String Id;
	
	@Column(name="Part2B")
	private String partToB;
	
	@Column(name="Part2T")
	private String partToT;
	
	@Column(name="Part1")
	private String part1;
	
	@Column(name="Scope")
	private String scope;
	
	@Column(name="Language_Type")
	private String languageType;
	
	@Column(name="Ref_Name")
	private String refName;
	
	@Column(name="Comment")
	private String comment;

}
