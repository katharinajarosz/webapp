package com.hw.database.beans.processcontrol;

import javax.annotation.ManagedBean;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * The system state subsequence bean.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Getter @Setter
@Builder @NoArgsConstructor @AllArgsConstructor
@Entity
@Table(name = "SystemStateSubsequence", schema = "machine")
@ManagedBean
public class SystemStateSubsequence {
	
	@Id
	@Column(name = "Subsequence_Node",columnDefinition = "HIERARCHYID")
	private byte[] subsequenceNode;
	
	@Column(name="Subsequence_Level", columnDefinition = "AS Subsequence_Node.GetLevel()")
	private int subsequenceLevel;
	
	@Column(name="Subsequence_ID")
	private int subsequenceId;
	
	@Column(name="Subsequence_Name")
	private String subsequenceName;
	
	@Column(name = "Subsequence_Node_String", columnDefinition = "AS Subsequence_Node.ToString()")
	private String nodeString;		
		
}
