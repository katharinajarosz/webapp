package com.hw.importservice.converter;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.AttributeConverter;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import com.google.common.base.Optional;
import com.hw.database.beans.globaldata.Address;
import com.hw.database.beans.globaldata.CalibrationData;
import com.hw.database.beans.globaldata.Clinics;
import com.hw.database.beans.globaldata.MachineRun;
import com.hw.database.beans.processcontrol.SystemState;
import com.hw.database.beans.processcontrol.SystemStateSubsequence;
import com.hw.parser.beans.RHeaderData;

/**
 * Convert diverse process image beans into data base beans.
 * 
 * @author kaja
 * @version 2.0
 *
 */
@Component
public class MachineDataConverter {

	private ClinicsConverter clinicConverter;

	@Autowired
	private EntityManager em;

	/**
	 * Instantiates a new machine data converter.
	 */
	@Autowired
	public MachineDataConverter() {
		super();
		this.clinicConverter = new ClinicsConverter();
	}

	/**
	 * Creates the global data without header.
	 * 
	 * @param fn 
	 * @return the machine run
	 */
	public MachineRun createGlobalDataWithoutHeader(String fn) {

		return MachineRun.builder().fileName(fn).build();
	}
	

	/**
	 * Creates the clinic.
	 *
	 * @param rd
	 * @return the clinics
	 */
	public Clinics createClinic(RHeaderData rd) {

		return this.clinicConverter.convert(rd);

	}

	/**
	 * Creates the calibration data.
	 *
	 * @param hd the hd
	 * @return the list
	 */
	public List<CalibrationData> createCalibrationData(RHeaderData hd) {

		int number = hd.getCalibrationData().getNumberOfEntries();

		List<CalibrationData> calibrationData = new ArrayList<>();
		for (int a = 0; a < number; a++) {
			calibrationData.add(CalibrationData.builder()
					.type(CalibrationData.CalibrationType.values()[a])
					.value(hd.getCalibrationData().getCalibration().get(a).doubleValue()).build());
		}
		return calibrationData;

	}

	/**
	 * Create a native query to get single result of system state data base object with given parameters.
	 * 
	 * @param mainState
	 * @param subState
	 * @return an optional of system state
	 */
	public Optional<SystemState> getSystemStateNode(int mainState, int subState) {

		Query query = em.createNativeQuery("SELECT a.* FROM machine.system_state AS a " 
				+ "WHERE a.system_state_value =? "
				+ "AND a.system_state_node.GetAncestor(1) = (SELECT b.system_state_node "
				+ "FROM machine.system_state AS b " + "WHERE b.system_state_value =? "
				+ "AND b.system_state_node.GetLevel() =?);", SystemState.class);
		query.setParameter(1, subState);
		query.setParameter(2, mainState);
		query.setParameter(3, 1);

		SystemState node = null;
		try {
			node = (SystemState) query.getSingleResult();
		} catch (NoResultException e) {

		}
		return Optional.fromNullable(node);
	}

	/**
	 * Create a native query to get single result of system state subsequence data base object with given parameters.
	 * 
	 * @param subsequence
	 * @param subsequenceState
	 * @return an optional of system state subsequence
	 */
	public Optional<SystemStateSubsequence> getSystemSubsequenceNode(int subsequence, int subsequenceState) {

		Query query = em.createNativeQuery("SELECT a.* FROM machine.system_state_subsequence AS a "
				+ "WHERE a.subsequence_node.GetAncestor(1) = (SELECT b.subsequence_node "
				+ "FROM machine.system_state_subsequence AS b " + "WHERE b.subsequence_id =? " 
				+ "AND b.subsequence_level =? ) "
				+ "AND a.subsequence_id =?;", SystemStateSubsequence.class);
		query.setParameter(1, subsequence);
		query.setParameter(2, 1);
		query.setParameter(3, subsequenceState);

		SystemStateSubsequence node = null;
		try {
			node = (SystemStateSubsequence) query.getSingleResult();
		} catch (NoResultException e) {

		}
		return Optional.fromNullable(node);
	}

	/**
	 * Read the machine number from the given string and converts it to an integer.
	 * 
	 * @author kaja
	 * @version 1.0
	 *
	 */
	public class MachineNumberConverter implements Converter<String, Integer> {
		
		@Override
		public Integer convert(String source) {

			String[] splitResult = source.split("_");
			String lk = splitResult[6];
			String[] lkSplit = lk.split("-");
			String number = lkSplit[1];
			int machineNumber = Integer.parseInt(number);
			return machineNumber;
		}

	}

	/**
	 * Converts a {@link RHeaderData} object to a {@link Clinics} object.
	 * 
	 * @author kaja
	 * @version 1.0
	 *
	 */
	public class ClinicsConverter implements Converter<RHeaderData, Clinics> {
		
		@Override
		public Clinics convert(RHeaderData source) {

			Clinics cl = null;
			String clinicSource = source.getSystemConfiguration().getClinicName();

			String clinic = clinicSource.trim();
			if (clinic.length() > 1) {

				String[] splitResult = clinic.split(",");
				String clinicName = splitResult[0].trim();
				String[] streetSource = splitResult[1].trim().split(" ");
				int length = streetSource.length;
				String number = streetSource[length - 1];
				List<String> streetX = new ArrayList<>();
				for (int i = 0; i < length - 1; i++) {
					streetX.add(streetSource[i]);
				}
				String street = String.join(" ", streetX);
				String[] plzCity = splitResult[2].trim().split(" ");
				int plz = Integer.parseInt(plzCity[0].trim());
				String city = plzCity[1].trim();
				cl = Clinics.builder().clinicName(clinicName)
						.addr(Address.builder().street(street).number(number).plz(plz).city(city).build()).build();
			}
			return cl;
		}

	}

	/**
	 * @author kaja
	 * @version 1.0
	 *
	 */
	public class HierarchyidTypeConverter implements AttributeConverter<String, Byte[]> {

		/**
		 * @param is
		 * @return string node
		 */
		public String convertToString(int... is) {
			String node = "/";
			for (int i : is) {
				node = node + i + node;
			}
			return node;
		}

		@Override
		public Byte[] convertToDatabaseColumn(String attribute) {

			Query query = em.createNativeQuery(
					"SELECT system_state_node FROM system_state WHERE system_state_node.ToString() =?;",
					Byte[].class);
			query.setParameter(1, attribute);

			Byte[] node = null;
			try {
				node = (Byte[]) query.getSingleResult();
			} catch (NoResultException e) {

			}
			return node;
		}

		@Override
		public String convertToEntityAttribute(Byte[] dbData) {

			Query query = em.createNativeQuery(
					"SELECT system_state_node.ToString() FROM system_state WHERE system_state_node =?;",
					String.class);
			query.setParameter(1, dbData);

			String node = null;
			try {
				node = (String) query.getSingleResult();
			} catch (NoResultException e) {

			}
			return node;
		}

	}

}
