package com.hw.importservice.converter;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

/**
 * Converts a {@link Long} of LabView time object into {@link LocalDateTime}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
public class LocalDateTimeConverter {
	
	private static final long LAB_VIEW_UTC = -2082844800;

	/**
	 * @param source
	 * @param nano
	 * @return a local timestamp
	 */
	public LocalDateTime convert(Long source, Long nano) {

		long target = LAB_VIEW_UTC + source;
		Instant instant = Instant.ofEpochSecond(source, nano);
		int nanoSeconds = instant.getNano();
		LocalDateTime localTimestamp = LocalDateTime.ofEpochSecond(target, nanoSeconds, ZoneOffset.ofHours(1));

		return localTimestamp;
	}


}