package com.hw.importservice.converter;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Map;

import com.microsoft.sqlserver.jdbc.SQLServerDataColumn;
import com.microsoft.sqlserver.jdbc.SQLServerDataTable;
import com.microsoft.sqlserver.jdbc.SQLServerException;
import com.microsoft.sqlserver.jdbc.SQLServerPreparedStatement;


// TODO: Auto-generated Javadoc
/**
 * Add column meta data to 'in memory' SQL tables and executed the 'insert into' SQL statement.
 * 
 * @author kaja
 * @version 1.0
 *
 */
public class TemporaryTables {

		private Connection conn;
		private static final String DB_URL = "jdbc:sqlserver://localhost;databaseName=Machine_Data_DB;integratedSecurity=true";
		
		// @Value("${spring.datasource.url}")
		private static final String DB_URL_0 = "${spring.datasource.url}";

		/**
		 * Instantiates a new temporary tables.
		 */
		public TemporaryTables() {
			this.conn = this.getConnection();
		}

		private Connection getConnection() {
			Connection conn = null;
			try {
				conn = DriverManager.getConnection(DB_URL);
			} catch (SQLException e) {
				e.printStackTrace();
			}

			return conn;
		}

		/**
		 * Adds the system diagnosis meta data.
		 *
		 * @param ssdt the ssdt
		 * @return the SQL server data table
		 * @throws SQLServerException the SQL server exception
		 */
		public SQLServerDataTable addSystemDiagnosisMetaData(SQLServerDataTable ssdt) throws SQLServerException {
			Map<Integer, SQLServerDataColumn> columnMetadata = ssdt.getColumnMetadata();
			if(columnMetadata.isEmpty()) {
				ssdt.addColumnMetadata("machine_data_loop_timestamp", java.sql.Types.TIMESTAMP);
				ssdt.addColumnMetadata("host_lk_bytes", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("host_lk_processor_load", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("host_reconnect_counter", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("host_processor_load", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("loop_late_counter", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("memory_load", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("processor_load", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("system_loop_processing_time", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("machine_run_id", java.sql.Types.BIGINT);
			}			
			return ssdt;
		}

		/**
		 * Adds the balancing meta data.
		 *
		 * @param ssdt the ssdt
		 * @return the SQL server data table
		 * @throws SQLServerException the SQL server exception
		 */
		public SQLServerDataTable addBalancingMetaData(SQLServerDataTable ssdt) throws SQLServerException {
			Map<Integer, SQLServerDataColumn> columnMetadata = ssdt.getColumnMetadata();
			if(columnMetadata.isEmpty()) {
				ssdt.addColumnMetadata("machine_data_loop_timestamp", java.sql.Types.TIMESTAMP);
				ssdt.addColumnMetadata("acid_flow_conrol", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("acid_weight", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("base_flow_control", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("base_weight", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("scale1row", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("scale2row", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("scale_deviation", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("scale_sum_current", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("uf_control", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("uf_host", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("uf_weight", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("balance_offset", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("machine_run_id", java.sql.Types.BIGINT);
			}			
			return ssdt;
		}

		/**
		 * Adds the analog in meta data.
		 *
		 * @param ssdt the ssdt
		 * @return the SQL server data table
		 * @throws SQLServerException the SQL server exception
		 */
		public SQLServerDataTable addAnalogInMetaData(SQLServerDataTable ssdt) throws SQLServerException {
			Map<Integer, SQLServerDataColumn> columnMetadata = ssdt.getColumnMetadata();
			if(columnMetadata.isEmpty()) {
				ssdt.addColumnMetadata("machine_data_loop_timestamp", java.sql.Types.TIMESTAMP);
				ssdt.addColumnMetadata("type", java.sql.Types.VARCHAR);
				ssdt.addColumnMetadata("converted_data", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("filtered_data", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("machine_run_id", java.sql.Types.BIGINT);
			}			
			return ssdt;
		}

		/**
		 * Adds the pump meta data.
		 *
		 * @param ssdt the ssdt
		 * @return the SQL server data table
		 * @throws SQLServerException the SQL server exception
		 */
		public SQLServerDataTable addPumpMetaData(SQLServerDataTable ssdt) throws SQLServerException {
			Map<Integer, SQLServerDataColumn> columnMetadata = ssdt.getColumnMetadata();
			if(columnMetadata.isEmpty()) {
				ssdt.addColumnMetadata("machine_data_loop_timestamp", java.sql.Types.TIMESTAMP);
				ssdt.addColumnMetadata("type", java.sql.Types.VARCHAR);
				ssdt.addColumnMetadata("control", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("host", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("measure", java.sql.Types.DOUBLE);				
				ssdt.addColumnMetadata("machine_run_id", java.sql.Types.BIGINT);
			}			
			return ssdt;
		}

		/**
		 * Adds the sond meta data.
		 *
		 * @param ssdt the ssdt
		 * @return the SQL server data table
		 * @throws SQLServerException the SQL server exception
		 */
		public SQLServerDataTable addSondMetaData(SQLServerDataTable ssdt) throws SQLServerException {
			Map<Integer, SQLServerDataColumn> columnMetadata = ssdt.getColumnMetadata();
			if(columnMetadata.isEmpty()) {
				ssdt.addColumnMetadata("machine_data_loop_timestamp", java.sql.Types.TIMESTAMP);
				ssdt.addColumnMetadata("type", java.sql.Types.VARCHAR);
				ssdt.addColumnMetadata("calculated_ph", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("calculated_temperature", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("measured_temperature", java.sql.Types.INTEGER);
				ssdt.addColumnMetadata("measured_ph", java.sql.Types.INTEGER);																
				ssdt.addColumnMetadata("machine_run_id", java.sql.Types.BIGINT);
			}			
			return ssdt;
		}

		/**
		 * Adds the closed loop meta data.
		 *
		 * @param ssdt the ssdt
		 * @return the SQL server data table
		 * @throws SQLServerException the SQL server exception
		 */
		public SQLServerDataTable addClosedLoopMetaData(SQLServerDataTable ssdt) throws SQLServerException {
			Map<Integer, SQLServerDataColumn> columnMetadata = ssdt.getColumnMetadata();
			if(columnMetadata.isEmpty()) {
				ssdt.addColumnMetadata("Machine_Data_Loop_Timestamp", java.sql.Types.TIMESTAMP);
				ssdt.addColumnMetadata("DEM", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("E", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("HMI", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("INP", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("PROCX", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("Y", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("Machine_Run_Id", java.sql.Types.BIGINT);
				ssdt.addColumnMetadata("Closed_Loop_Type_ID", java.sql.Types.INTEGER);
			}			
			return ssdt;
		}

		/**
		 * Adds the digital input meta data.
		 *
		 * @param ssdt the ssdt
		 * @return the SQL server data table
		 * @throws SQLServerException the SQL server exception
		 */
		public SQLServerDataTable addDigitalInputMetaData(SQLServerDataTable ssdt) throws SQLServerException {
			Map<Integer, SQLServerDataColumn> columnMetadata = ssdt.getColumnMetadata();
			if(columnMetadata.isEmpty()) {
				ssdt.addColumnMetadata("Machine_Data_Loop_Timestamp", java.sql.Types.TIMESTAMP);				
				ssdt.addColumnMetadata("CD", java.sql.Types.BIT);
				ssdt.addColumnMetadata("R", java.sql.Types.BIT);
				ssdt.addColumnMetadata("Machine_Run_Id", java.sql.Types.BIGINT);
				ssdt.addColumnMetadata("Digital_Input_Type_ID", java.sql.Types.INTEGER);
			}			
			return ssdt;
		}

		/**
		 * Adds the digital output meta data.
		 *
		 * @param ssdt the ssdt
		 * @return the SQL server data table
		 * @throws SQLServerException the SQL server exception
		 */
		public SQLServerDataTable addDigitalOutputMetaData(SQLServerDataTable ssdt) throws SQLServerException {
			Map<Integer, SQLServerDataColumn> columnMetadata = ssdt.getColumnMetadata();
			if(columnMetadata.isEmpty()) {
				ssdt.addColumnMetadata("Machine_Data_Loop_Timestamp", java.sql.Types.TIMESTAMP);					
				ssdt.addColumnMetadata("DEM", java.sql.Types.BIT);
				ssdt.addColumnMetadata("HMI", java.sql.Types.BIT);
				ssdt.addColumnMetadata("Machine_Run_Id", java.sql.Types.BIGINT);
				ssdt.addColumnMetadata("Digital_Output_Type_ID", java.sql.Types.INTEGER);
			}			
			return ssdt;
		}

		/**
		 * Adds the numeric input meta data.
		 *
		 * @param ssdt the ssdt
		 * @return the SQL server data table
		 * @throws SQLServerException the SQL server exception
		 */
		public SQLServerDataTable addNumericInputMetaData(SQLServerDataTable ssdt) throws SQLServerException {
			Map<Integer, SQLServerDataColumn> columnMetadata = ssdt.getColumnMetadata();
			if(columnMetadata.isEmpty()) {
				ssdt.addColumnMetadata("Machine_Data_Loop_Timestamp", java.sql.Types.TIMESTAMP);
				ssdt.addColumnMetadata("CD", java.sql.Types.DOUBLE);				
				ssdt.addColumnMetadata("F", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("R", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("Machine_Run_Id", java.sql.Types.BIGINT);
				ssdt.addColumnMetadata("Numeric_Input_Type_ID", java.sql.Types.INTEGER);
			}			
			return ssdt;
		}

		/**
		 * Adds the numeric output meta data.
		 *
		 * @param ssdt the ssdt
		 * @return the SQL server data table
		 * @throws SQLServerException the SQL server exception
		 */
		public SQLServerDataTable addNumericOutputMetaData(SQLServerDataTable ssdt) throws SQLServerException {
			Map<Integer, SQLServerDataColumn> columnMetadata = ssdt.getColumnMetadata();
			if(columnMetadata.isEmpty()) {
				ssdt.addColumnMetadata("Machine_Data_Loop_Timestamp", java.sql.Types.TIMESTAMP);
				ssdt.addColumnMetadata("CD", java.sql.Types.DOUBLE);
				ssdt.addColumnMetadata("DEM", java.sql.Types.DOUBLE);				
				ssdt.addColumnMetadata("Machine_Run_Id", java.sql.Types.BIGINT);
				ssdt.addColumnMetadata("Numeric_Output_Type_ID", java.sql.Types.INTEGER);
			}			
			return ssdt;
		}

		/**
		 * Persist balancing data.
		 *
		 * @param ssdt the ssdt
		 * @throws SQLException the SQL exception
		 */
		public void persistBalancingData(SQLServerDataTable ssdt) throws SQLException {			

			SQLServerPreparedStatement pStmt = (SQLServerPreparedStatement) conn
					.prepareStatement("INSERT INTO machine.balancing_data SELECT * FROM ?;");
			pStmt.setStructured(1, "machine.BalancingDataTemporary", ssdt);
			pStmt.execute();

			ssdt.clear();			
		}

		/**
		 * Persist system diagnosis.
		 *
		 * @param ssdt the ssdt
		 * @throws SQLException the SQL exception
		 */
		public void persistSystemDiagnosis(SQLServerDataTable ssdt) throws SQLException {			

			SQLServerPreparedStatement pStmt = (SQLServerPreparedStatement) conn
					.prepareStatement("INSERT INTO machine.system_diagnosis SELECT * FROM ?;");
			pStmt.setStructured(1, "machine.SystemDiagnosisTemporary", ssdt);
			pStmt.execute();

			ssdt.clear();
		}

		/**
		 * Persist analod in.
		 *
		 * @param ssdt the ssdt
		 * @throws SQLException the SQL exception
		 */
		public void persistAnalodIn(SQLServerDataTable ssdt) throws SQLException {			

			SQLServerPreparedStatement pStmt = (SQLServerPreparedStatement) conn
					.prepareStatement("INSERT INTO machine.analog_in SELECT * FROM ?;");
			pStmt.setStructured(1, "machine.AnalogInTemporary", ssdt);
			pStmt.execute();

			ssdt.clear();
		}

		/**
		 * Persist pump data.
		 *
		 * @param ssdt the ssdt
		 * @throws SQLException the SQL exception
		 */
		public void persistPumpData(SQLServerDataTable ssdt) throws SQLException {			

			SQLServerPreparedStatement pStmt = (SQLServerPreparedStatement) conn
					.prepareStatement("INSERT INTO machine.pump_data SELECT * FROM ?;");
			pStmt.setStructured(1, "machine.PumpDataTemporary", ssdt);
			pStmt.execute();

			ssdt.clear();
		}

		/**
		 * Persist sond data.
		 *
		 * @param ssdt the ssdt
		 * @throws SQLException the SQL exception
		 */
		public void persistSondData(SQLServerDataTable ssdt) throws SQLException {			

			SQLServerPreparedStatement pStmt = (SQLServerPreparedStatement) conn
					.prepareStatement("INSERT INTO machine.sond_data SELECT * FROM ?;");
			pStmt.setStructured(1, "machine.SondDataTemporary", ssdt);
			pStmt.execute();

			ssdt.clear();
		}

		/**
		 * Persist closed loop.
		 *
		 * @param ssdt the ssdt
		 * @throws SQLException the SQL exception
		 */
		public void persistClosedLoop(SQLServerDataTable ssdt) throws SQLException {			

			SQLServerPreparedStatement pStmt = (SQLServerPreparedStatement) conn
					.prepareStatement("INSERT INTO machine.closed_loop SELECT * FROM ?;");
			pStmt.setStructured(1, "machine.ClosedLoopTemporary", ssdt);
			pStmt.execute();

			ssdt.clear();
		}

		/**
		 * Persist digital input.
		 *
		 * @param ssdt the ssdt
		 * @throws SQLException the SQL exception
		 */
		public void persistDigitalInput(SQLServerDataTable ssdt) throws SQLException {			

			SQLServerPreparedStatement pStmt = (SQLServerPreparedStatement) conn
					.prepareStatement("INSERT INTO machine.digital_input SELECT * FROM ?;");
			pStmt.setStructured(1, "machine.DigitalInputTemporary", ssdt);
			pStmt.execute();

			ssdt.clear();
		}

		/**
		 * Persist digital output.
		 *
		 * @param ssdt the ssdt
		 * @throws SQLException the SQL exception
		 */
		public void persistDigitalOutput(SQLServerDataTable ssdt) throws SQLException {			

			SQLServerPreparedStatement pStmt = (SQLServerPreparedStatement) conn
					.prepareStatement("INSERT INTO machine.digital_output SELECT * FROM ?;");
			pStmt.setStructured(1, "machine.DigitalOutputTemporary", ssdt);
			pStmt.execute();

			ssdt.clear();
		}

		/**
		 * Persist numeric input.
		 *
		 * @param ssdt the ssdt
		 * @throws SQLException the SQL exception
		 */
		public void persistNumericInput(SQLServerDataTable ssdt) throws SQLException {			

			SQLServerPreparedStatement pStmt = (SQLServerPreparedStatement) conn
					.prepareStatement("INSERT INTO machine.numeric_input SELECT * FROM ?;");
			pStmt.setStructured(1, "machine.NumericInputTemporary", ssdt);
			pStmt.execute();

			ssdt.clear();
		}

		/**
		 * Persist numeric output.
		 *
		 * @param ssdt the ssdt
		 * @throws SQLException the SQL exception
		 */
		public void persistNumericOutput(SQLServerDataTable ssdt) throws SQLException {			

			SQLServerPreparedStatement pStmt = (SQLServerPreparedStatement) conn
					.prepareStatement("INSERT INTO machine.numeric_output SELECT * FROM ?;");
			pStmt.setStructured(1, "machine.NumericOutputTemporary", ssdt);
			pStmt.execute();

			ssdt.clear();
		}
	}