package com.hw.importservice;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManagerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import com.google.common.base.Optional;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.hw.daos.ClosedLoopTypeRepository;
import com.hw.daos.DigitalInputTypeRepository;
import com.hw.daos.DigitalOutputTypeRepository;
import com.hw.daos.ErrorRepository;
import com.hw.daos.GpModeTypeRepository;
import com.hw.daos.HeaterModeTypeRepository;
import com.hw.daos.MachineRunRepository;
import com.hw.daos.NumericInputTypeRepository;
import com.hw.daos.NumericOutputTypeRepository;
import com.hw.daos.PpSupplyModeTypeRepository;
import com.hw.daos.PpWasteModeTypeRepository;
import com.hw.daos.PsConfigurationRepository;
import com.hw.daos.SeverityRepository;
import com.hw.database.beans.controlsystem.ClosedLoop;
import com.hw.database.beans.controlsystem.DigitalInput;
import com.hw.database.beans.controlsystem.DigitalOutput;
import com.hw.database.beans.controlsystem.IOData;
import com.hw.database.beans.controlsystem.NumericInput;
import com.hw.database.beans.controlsystem.NumericOutput;
import com.hw.database.beans.controlsystem.types.ClosedLoopType;
import com.hw.database.beans.controlsystem.types.DigitalInputType;
import com.hw.database.beans.controlsystem.types.DigitalOutputType;
import com.hw.database.beans.controlsystem.types.GpModeType;
import com.hw.database.beans.controlsystem.types.HeaterModeType;
import com.hw.database.beans.controlsystem.types.NumericInputType;
import com.hw.database.beans.controlsystem.types.NumericOutputType;
import com.hw.database.beans.controlsystem.types.PpSupplyModeType;
import com.hw.database.beans.controlsystem.types.PpWasteModeType;
import com.hw.database.beans.errordata.ActiveError;
import com.hw.database.beans.errordata.Error;
import com.hw.database.beans.errordata.Severity;
import com.hw.database.beans.globaldata.MachineRun;
import com.hw.database.beans.processcontrol.ProtectState;
import com.hw.database.beans.processcontrol.RunningSystemState;
import com.hw.database.beans.processcontrol.SystemState;
import com.hw.database.beans.processcontrol.SystemStateSubsequence;
import com.hw.database.beans.protectivesystem.AnalogIn.AnalogInType;
import com.hw.database.beans.protectivesystem.PSConfiguration;
import com.hw.database.beans.protectivesystem.PSMissedMessage;
import com.hw.database.beans.protectivesystem.PSStatus;
import com.hw.database.beans.protectivesystem.PumpData.PumpDataType;
import com.hw.database.beans.protectivesystem.SondData.SondDataType;
import com.hw.importservice.converter.MachineDataConverter;
import com.hw.importservice.converter.TemporaryTables;
import com.hw.importservice.converter.TimestampConverter;
import com.hw.importservice.filereader.HwBinFileReader;
import com.hw.parser.beans.RHeaderData;
import com.hw.parser.beans.RMachineDataSet;
import com.hw.parser.beans.machinedata.RClosedLoop;
import com.hw.parser.beans.machinedata.RDigitalInput;
import com.hw.parser.beans.machinedata.RDigitalOutput;
import com.hw.parser.beans.machinedata.RErrorElement;
import com.hw.parser.beans.machinedata.RNumericInput;
import com.hw.parser.beans.machinedata.RNumericOutput;
import com.microsoft.sqlserver.jdbc.SQLServerDataTable;
import com.microsoft.sqlserver.jdbc.SQLServerException;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * The Class ImportServiceInMemoryTables.
 *
 * @author kaja
 * @version 2.0
 * 
 */
@Service
@EnableScheduling
@Slf4j
public class ImportServiceInMemoryTables {

	private static Path hwBin = Paths.get("\\\\", "NAS01", "MachineData", "RohData_LK2001");
	FileTime currentYear = FileTime.from(Instant.parse("2020-01-01T00:00:00.00Z"));	
	private static final int HEADER_SIZE = 1024;
	private static final String FILE_PATTERN = "glob:*.hwbin";
	private static final String FILE_REGEX = ".*([0-9]\\.[0-9]\\.[0-9].*[FDC].*)";
	private static final int[] numericInputIds = { 8, 9, 10, 11, 12, 14, 34, 35, 36, 52, 53, 55, 56, 57, 58, 59 };
	private static final int[] closedLoopIds = { 3, 4, 21, 22, 24, 25, 26, 27 };

	@Getter
	@Setter
	private RMachineDataSet rMachineDataSet;
	@Getter
	@Setter
	private RHeaderData headerData;
	@Getter
	@Setter
	private MachineRun machineRun;
	private TimestampConverter timestampConverter;
	private List<RClosedLoop> closedLoopList = new ArrayList<>();
	private List<RDigitalInput> digitalInputList = new ArrayList<>();
	private List<RDigitalOutput> digitalOutputList = new ArrayList<>();
	private List<RNumericInput> numericInputList = new ArrayList<>();
	private List<RNumericOutput> numericOutputList = new ArrayList<>();
	private SQLServerDataTable tempNumInputs;
	private SQLServerDataTable tempNumOutputs;
	private SQLServerDataTable tempDigInputs;
	private SQLServerDataTable tempDigOutputs;
	private SQLServerDataTable tempClosedloops;
	private SQLServerDataTable tempBalancingData;
	private SQLServerDataTable tempSysDiagnosis;
	private SQLServerDataTable tempAnalogIn;
	private SQLServerDataTable tempPumpData;
	private SQLServerDataTable tempSondData;
	private TemporaryTables temporary;
	private HwBinFileReader hwBinFileReader;
	private ByteBuffer byteBuffer;
	private final PathMatcher matcher;
	private Queue<Path> pathQueue;
	byte[] intByteArr = new byte[4];
	private SystemStateSubsequence[][] systemStateWithSubstate = new SystemStateSubsequence[40][21];
	private SystemState[][] systemState = new SystemState[7][92];
	private List<Integer> numInIds = Arrays.stream(numericInputIds).boxed().collect(Collectors.toList());
	private List<Integer> clLoopIds = Arrays.stream(closedLoopIds).boxed().collect(Collectors.toList());

	@Autowired
	private ErrorRepository errorRepo;
	@Autowired
	private SeverityRepository severityRepo;
	@Autowired
	private MachineDataConverter machineDataConverter;
	@Autowired
	private MachineRunRepository machineRunRepo;
	@Autowired
	private ClosedLoopTypeRepository closedLoopTypeRepo;
	@Autowired
	private DigitalInputTypeRepository digitalInputTypeRepo;
	@Autowired
	private DigitalOutputTypeRepository digitalOutputTypeRepo;
	@Autowired
	private NumericInputTypeRepository numericInputTypeRepo;
	@Autowired
	private NumericOutputTypeRepository numericOutputTypeRepo;
	@Autowired
	private GpModeTypeRepository gpModeTypeRepo;
	@Autowired
	private HeaterModeTypeRepository heaterModeTypeRepo;
	@Autowired
	private PpSupplyModeTypeRepository supplyModeTypeRepo;
	@Autowired
	private PpWasteModeTypeRepository wasteModeTypeRepo;
	@Autowired
	private PsConfigurationRepository psConfigRepo;

	/**
	 * Instantiates a new import service in memory tables.
	 *
	 * @param emf the entity manager factory
	 */
	@Autowired
	public ImportServiceInMemoryTables(EntityManagerFactory emf) {
		super();
		this.timestampConverter = new TimestampConverter();
		this.hwBinFileReader = new HwBinFileReader();
		this.pathQueue = new ConcurrentLinkedQueue<>();
		this.temporary = new TemporaryTables();
		Assert.notNull(emf, "EntityManagerFactory must not be null");
		matcher = FileSystems.getDefault().getPathMatcher(FILE_PATTERN);
	}

	/**
	 * Build matrices for system states and sub-states.
	 *
	 * @throws SQLServerException the SQL server exception
	 */
	@PostConstruct
	private void init() throws SQLServerException {
		log.info("init matrix started");
		systemState = this.initStateMatrix();
		systemStateWithSubstate = this.initStateMatrixWithSubstate();
		log.info("init matrix completed");
	}

	/**
	 * Fill in a two-dimensional array with system states and sub-states.
	 *
	 * @return the two-dimensional integer array
	 */
	private SystemState[][] initStateMatrix() {
		for (int i = 0; i <= 6; i++)
			for (int j = 0; j <= 91; j++) {
				Optional<SystemState> state = machineDataConverter.getSystemStateNode(i, j);
				if (state.isPresent()) {
					systemState[i][j] = state.get();
				}
			}
		return systemState;
	}

	/**
	 * Fill in a four-dimensional array with all system states, sub-states and its
	 * steps.
	 *
	 * @return the four-dimensional integer array
	 */
	private SystemStateSubsequence[][] initStateMatrixWithSubstate() {
		for (int i = 0; i <= 39; i++) {
			for (int j = 0; j <= 20; j++) {
				Optional<SystemStateSubsequence> state = machineDataConverter.getSystemSubsequenceNode(i, j);
				if (state.isPresent()) {
					systemStateWithSubstate[i][j] = state.get();

				}
			}
		}
		return systemStateWithSubstate;
	}

	/**
	 * Scan file system.
	 */
	@Scheduled(fixedRateString = "P1D") // 22,22 h
	public void scanFileSystem() {

		@SuppressWarnings("unused")
		int i = 0;

		try {
			HWBinFileFinder hwBinFileFinder = new HWBinFileFinder();
			Files.walkFileTree(hwBin, hwBinFileFinder);
			while (hwBinFileFinder.getMachineDataSets()) {

			}

		} catch (IOException | InterruptedException e) {

			e.printStackTrace();
		}
	}

	/**
	 * The Class HWBinFileFinder.
	 * 
	 * Deploy methods of {@link SimpleFileVisitor} for HWBIN file system.
	 * <p>
	 * Read machine data from HWBIN files, convert and persist this data into data
	 * base.
	 * <p>
	 * The data are first loaded into in memory tables and than imported in bulk
	 * into the data base.
	 * 
	 * @author kaja
	 * @version 1.0
	 *
	 */
	public class HWBinFileFinder extends SimpleFileVisitor<Path> {

		/**
		 * Skip subtrees that contain invalid files.
		 *
		 * @param file  the file
		 * @param attrs the attrs
		 * @return the file visit result
		 */
		@Override
		public FileVisitResult preVisitDirectory(Path file, BasicFileAttributes attrs) {

			String name = file.getFileName().toString();

			switch (name) {

			case "007": 
			case "050": 
			case "051":
			case "055":
			case "852":			
			case "MP1": // doesn't contain LK2001 files
			case "MP2": // doesn't contain LK2001 files	
			case "MP3": // doesn't contain LK2001 files
			case "MP4": // doesn't contain LK2001 files	
				return FileVisitResult.SKIP_SUBTREE;

			}

			return FileVisitResult.CONTINUE;
		}

		/**
		 * If matches HWBIN file pattern put the files to a {@code Queue}.
		 *
		 * @param file  the file
		 * @param attrs the attrs
		 * @return the file visit result
		 * @throws IOException Signals that an I/O exception has occurred.
		 */
		@Override
		public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {

			Path name = file.getFileName();
			if (attrs.isRegularFile() && matcher.matches(name)) {

				FileTime created = attrs.creationTime();
				FileTime modified = attrs.lastModifiedTime();
				int c1 = created.compareTo(currentYear);
				int c2 = modified.compareTo(currentYear);
				if (c2 > 0) {
					log.info("File created or modified: " + name);
					
				}
				pathQueue.add(file);
				
			}

			return FileVisitResult.CONTINUE;
		}

		@Override
		public FileVisitResult visitFileFailed(Path file, IOException exc) throws IOException {

			return super.visitFileFailed(file, exc);
		}

		/**
		 * Initialized temporary SQL tables
		 * 
		 * @throws SQLServerException
		 */
		private void initTempTables() throws SQLServerException {
			tempNumInputs = temporary.addNumericInputMetaData(new SQLServerDataTable());
			tempNumOutputs = temporary.addNumericOutputMetaData(new SQLServerDataTable());
			tempDigInputs = temporary.addDigitalInputMetaData(new SQLServerDataTable());
			tempDigOutputs = temporary.addDigitalOutputMetaData(new SQLServerDataTable());
			tempClosedloops = temporary.addClosedLoopMetaData(new SQLServerDataTable());
			tempBalancingData = temporary.addBalancingMetaData(new SQLServerDataTable());
			tempSysDiagnosis = temporary.addSystemDiagnosisMetaData(new SQLServerDataTable());
			tempAnalogIn = temporary.addAnalogInMetaData(new SQLServerDataTable());
			tempPumpData = temporary.addPumpMetaData(new SQLServerDataTable());
			tempSondData = temporary.addSondMetaData(new SQLServerDataTable());
		}

		/**
		 * Read and persist machine data from HWBIN files into data base.
		 * 
		 * @return {@code true} if the file data has not yet persisted
		 * @throws InterruptedException
		 */
		private boolean getMachineDataSets() throws InterruptedException {			

			if (pathQueue.peek() != null) {
				Path element = pathQueue.poll();
				try (BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream(element.toFile()))) {

					String fileName = element.getFileName().toString();

					if (machineRunRepo.findByFileName(fileName) == null) {
						if (!fileName.matches(FILE_REGEX)) {													

							List<NumericInputType> numericInputTypes = numericInputTypeRepo.findAll();
							List<NumericOutputType> numericOutputTypes = numericOutputTypeRepo.findAll();
							List<DigitalInputType> digitalInputTypes = digitalInputTypeRepo.findAll();
							List<DigitalOutputType> digitalOutputTypes = digitalOutputTypeRepo.findAll();
							List<ClosedLoopType> closedLoopTypes = closedLoopTypeRepo.findAll();
							List<PpSupplyModeType> supplyModeTypes = supplyModeTypeRepo.findAll();
							List<PpWasteModeType> wasteModeTypes = wasteModeTypeRepo.findAll();
							List<GpModeType> gpModeTypes = gpModeTypeRepo.findAll();
							List<HeaterModeType> heaterModeTypes = heaterModeTypeRepo.findAll();
							List<Severity> severities = severityRepo.findAll();
							List<Error> errors = errorRepo.findAll();
							List<ActiveError> completeActiveErrors = new ArrayList<>();
							List<ActiveError> lastActiveErrors = new ArrayList<>();
							List<RErrorElement> currentErrorElements = new ArrayList<>();
							List<IOData> completeIoData = new ArrayList<>();
							IOData lastIoData = new IOData();
							List<PSMissedMessage> completePsMissedMessage = new ArrayList<>();
							PSMissedMessage lastMissedMessage = new PSMissedMessage();
							List<PSStatus> completePsStatus = new ArrayList<>();
							PSStatus lastPsStatus = new PSStatus();
							List<ProtectState> completeProtectStates = new ArrayList<>();
							ProtectState lastProtectStates = new ProtectState();
							List<RunningSystemState> completeSystemStates = new ArrayList<>();
							RunningSystemState lastSystemState = new RunningSystemState();
							Long lastTimestamp = 0l;
							Long lastTimestamp2 = 0l;
							int loop = 0;
							this.initTempTables();							
							boolean balanceOffset = this.matchVersion(fileName);

							machineRun = machineDataConverter.createGlobalDataWithoutHeader(fileName);
							machineRun = machineRunRepo.saveAndFlush(machineRun);														

							if (this.matchFileVersion(fileName)) {

								byte[] buff = new byte[HEADER_SIZE];
								inputStream.read(buff, 0, HEADER_SIZE);																								

							}

							// machine data set loop							
							int readBytes;
							while ((readBytes = inputStream.read(intByteArr)) != -1) {

								byte[] b = new byte[1];																

								int fromByteArray = ByteBuffer.wrap(intByteArr).getInt();																

								if (fromByteArray > 0) {

									byte[] data = new byte[fromByteArray];
									inputStream.read(data);
									byteBuffer = ByteBuffer.wrap(data);
									rMachineDataSet = hwBinFileReader.readMachineDataSet(byteBuffer, balanceOffset);
									byteBuffer.position(fromByteArray);
									byteBuffer.clear();
									inputStream.read(b);

									// create machine run start
									setMachineRunStart();

									

									tempBalancingData = createBalancingData(tempBalancingData);
									tempSysDiagnosis = createSystemDiagnosis(tempSysDiagnosis);
									tempAnalogIn = createAnalogIn(tempAnalogIn);
									tempPumpData = createPumpData(tempPumpData);
									tempSondData = createSondData(tempSondData);

									// create closed loops
									tempClosedloops = handleClosedLoops(closedLoopTypes, tempClosedloops);

									// create numeric inputs
									tempNumInputs = handleNumericInputs(numericInputTypes, tempNumInputs);

									// create digital inputs
									tempDigInputs = handleDigitalInputs(digitalInputTypes, tempDigInputs);

									// create numeric outputs
									tempNumOutputs = handleNumericOutputs(numericOutputTypes, tempNumOutputs);

									// create digital outputs
									tempDigOutputs = handleDigitalOutputs(digitalOutputTypes, tempDigOutputs);

									// create active errors
									currentErrorElements = getActiveErrorsOnly(currentErrorElements);

									// new errors
									lastActiveErrors = addNewErrors(lastActiveErrors, currentErrorElements, severities, errors);

									// canceled errors
									lastActiveErrors = handleCanceledErrors(completeActiveErrors, lastActiveErrors, currentErrorElements, lastTimestamp, lastTimestamp2);

									// create io data
									lastIoData = handleIoData(supplyModeTypes, wasteModeTypes, gpModeTypes,
											heaterModeTypes, completeIoData, lastIoData, lastTimestamp, lastTimestamp2);

									// create ps missed messages
									lastMissedMessage = handleMissedMessages(lastMissedMessage, completePsMissedMessage,
											lastTimestamp, lastTimestamp2);

									// create ps status
									lastPsStatus = handlePsStatus(lastPsStatus, completePsStatus, lastTimestamp,
											lastTimestamp2);

									// create protect state
									lastProtectStates = handleProtectStates(lastProtectStates, completeProtectStates,
											lastTimestamp, lastTimestamp2);

									// create system state
									lastSystemState = handleSystemStates(lastSystemState, completeSystemStates,
											lastTimestamp, lastTimestamp2);

									lastTimestamp = rMachineDataSet.getIoData().getTimestamp1();
									lastTimestamp2 = rMachineDataSet.getIoData().getTimestamp2();

									loop++;

									if (loop % 1000 == 0 && loop > 0) {

										temporary.persistAnalodIn(tempAnalogIn);

										temporary.persistBalancingData(tempBalancingData);

										temporary.persistClosedLoop(tempClosedloops);

										temporary.persistDigitalInput(tempDigInputs);

										temporary.persistDigitalOutput(tempDigOutputs);

										temporary.persistNumericInput(tempNumInputs);

										temporary.persistNumericOutput(tempNumOutputs);

										temporary.persistPumpData(tempPumpData);

										temporary.persistSondData(tempSondData);

										temporary.persistSystemDiagnosis(tempSysDiagnosis);

										this.initTempTables();

									}

								}
							}
							
							// create ps configuration
							setMachineRunPsConfig();

							// convert last timestamp
							Timestamp end = timestampConverter.convert(lastTimestamp, lastTimestamp2);

							// close all remaining errors
							completeActiveErrors = closeAllRemainingErrors(completeActiveErrors, lastActiveErrors, end);

							// set io data
							completeIoData = setIoData(completeIoData, lastIoData, end);

							// set missed messages
							completePsMissedMessage = setMissedMessages(completePsMissedMessage, lastMissedMessage);

							// set ps status
							completePsStatus = setPsStatus(completePsStatus, lastPsStatus);

							// set protect state
							completeProtectStates = setProtectStates(completeProtectStates, lastProtectStates);

							// set system state
							completeSystemStates = setSystemStates(completeSystemStates, lastSystemState);

							// create machine run end and set lists
							machineRun.setActiveErrors(completeActiveErrors);							
							machineRun.setProtectStates(completeProtectStates);
							machineRun.setPsStatus(completePsStatus);
							machineRun.setPsMissedMessage(completePsMissedMessage);
							machineRun.setIoData(completeIoData);
							
							machineRun.setSystemStates(completeSystemStates);
							machineRun.setEnd(end);													

							machineRunRepo.save(machineRun);														

							temporary.persistAnalodIn(tempAnalogIn);
							temporary.persistBalancingData(tempBalancingData);
							temporary.persistClosedLoop(tempClosedloops);
							temporary.persistDigitalInput(tempDigInputs);
							temporary.persistDigitalOutput(tempDigOutputs);
							temporary.persistNumericInput(tempNumInputs);
							temporary.persistNumericOutput(tempNumOutputs);
							temporary.persistPumpData(tempPumpData);
							temporary.persistSondData(tempSondData);
							temporary.persistSystemDiagnosis(tempSysDiagnosis);

							log.info("Machine ID: " + machineRun.getID() + " saved!");

							machineRun = null;
						}
					}
				} catch (IOException | SQLException e) {

					e.printStackTrace();
				}
				
				return true;
			}

			return false;
		}

		/**
		 * Creates the system diagnosis.
		 *
		 * @param ssdt the SQL server data table
		 * @return the SQL server data table
		 * @throws SQLServerException the SQL server exception
		 */
		private SQLServerDataTable createSystemDiagnosis(SQLServerDataTable ssdt) throws SQLServerException {

			ssdt.addRow(
					timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
							rMachineDataSet.getIoData().getTimestamp2()),
					rMachineDataSet.getSystemDiagnosis().getHostLK2001PrivateBytes(),
					rMachineDataSet.getSystemDiagnosis().getHostLK2001ProcessorLoad(),
					rMachineDataSet.getSystemDiagnosis().getHostReconnectCounter(),
					rMachineDataSet.getSystemDiagnosis().getHostTotalProcessorLoad(),
					rMachineDataSet.getSystemDiagnosis().getLoopLateCounter(),
					rMachineDataSet.getSystemDiagnosis().getMemoryUsage(),
					rMachineDataSet.getSystemDiagnosis().getProcessorLoad(),
					rMachineDataSet.getSystemDiagnosis().getSystemLoopProcessingTime(), machineRun.getID());
			return ssdt;

		}

		/**
		 * Creates the balancing data.
		 *
		 * @param ssdt the SQL server data table
		 * @return the SQL server data table
		 * @throws SQLServerException the SQL server exception
		 */
		private SQLServerDataTable createBalancingData(SQLServerDataTable ssdt) throws SQLServerException {

			ssdt.addRow(
					timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
							rMachineDataSet.getIoData().getTimestamp2()),
					rMachineDataSet.getProtectiveSystemData().getBalancingData().getAcidFlowConrol(),
					rMachineDataSet.getProtectiveSystemData().getBalancingData().getAcidWeight(),
					rMachineDataSet.getProtectiveSystemData().getBalancingData().getBaseFlowControl(),
					rMachineDataSet.getProtectiveSystemData().getBalancingData().getBaseWeight(),
					rMachineDataSet.getProtectiveSystemData().getBalancingData().getScale1row(),
					rMachineDataSet.getProtectiveSystemData().getBalancingData().getScale2row(),
					rMachineDataSet.getProtectiveSystemData().getBalancingData().getScaleDeviation(),
					rMachineDataSet.getProtectiveSystemData().getBalancingData().getScaleSumCurrent(),
					rMachineDataSet.getProtectiveSystemData().getBalancingData().getUfControl(),
					rMachineDataSet.getProtectiveSystemData().getBalancingData().getUfHost(),
					rMachineDataSet.getProtectiveSystemData().getBalancingData().getBalanceOffset(),
					rMachineDataSet.getProtectiveSystemData().getBalancingData().getUfWeight(), machineRun.getID());
			return ssdt;

		}

		/**
		 * Creates the analog in.
		 *
		 * @param ssdt the SQL server data table
		 * @return the SQL server data table
		 * @throws SQLServerException the SQL server exception
		 */
		private SQLServerDataTable createAnalogIn(SQLServerDataTable ssdt) throws SQLServerException {

			for (int a = 0; a < 10; a++) {
				ssdt.addRow(
						timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
								rMachineDataSet.getIoData().getTimestamp2()),
						AnalogInType.values()[a].getType(),
						rMachineDataSet.getProtectiveSystemData().getAnalogIn().get(a).getCd(),
						rMachineDataSet.getProtectiveSystemData().getAnalogIn().get(a).getF(), machineRun.getID());
			}
			return ssdt;
		}

		/**
		 * Creates the pump data.
		 *
		 * @param ssdt the SQL server data table
		 * @return the SQL server data table
		 * @throws SQLServerException the SQL server exception
		 */
		private SQLServerDataTable createPumpData(SQLServerDataTable ssdt) throws SQLServerException {

			for (int a = 0; a < 2; a++) {
				ssdt.addRow(
						timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
								rMachineDataSet.getIoData().getTimestamp2()),
						PumpDataType.values()[a].getType(),
						rMachineDataSet.getProtectiveSystemData().getPumpData().get(a).getControl(),
						rMachineDataSet.getProtectiveSystemData().getPumpData().get(a).getHost(),
						rMachineDataSet.getProtectiveSystemData().getPumpData().get(a).getMeas(), machineRun.getID());

			}
			return ssdt;
		}

		/**
		 * Creates the sond data.
		 *
		 * @param ssdt the SQL server data table
		 * @return the SQL server data table
		 * @throws SQLServerException the SQL server exception
		 */
		private SQLServerDataTable createSondData(SQLServerDataTable ssdt) throws SQLServerException {

			for (int a = 0; a < 4; a++) {
				ssdt.addRow(
						timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
								rMachineDataSet.getIoData().getTimestamp2()),
						SondDataType.values()[a].getType(),
						rMachineDataSet.getProtectiveSystemData().getSondData().get(a).getPh(),
						rMachineDataSet.getProtectiveSystemData().getSondData().get(a).getTemperature(),
						rMachineDataSet.getProtectiveSystemData().getSondData().get(a).getUTemp(),
						rMachineDataSet.getProtectiveSystemData().getSondData().get(a).getUpH(), machineRun.getID());

			}
			return ssdt;
		}

		/**
		 * Handle I/O data.
		 *
		 * @param supplyModeTypes          the supply mode types
		 * @param wasteModeTypes           the waste mode types
		 * @param gpModeTypes              the gp mode types
		 * @param heaterModeTypes          the heater mode types
		 * @param lastIoData               the last io data
		 * @param lastIoDataTimestampStart the last io data timestamp start
		 * @param lastIoDataTimestampEnd   the last io data timestamp end
		 * @param lastTimestamp            the last timestamp
		 * @return the list of I/O data
		 */
		private IOData handleIoData(List<PpSupplyModeType> supplyModeTypes, List<PpWasteModeType> wasteModeTypes,
				List<GpModeType> gpModeTypes, List<HeaterModeType> heaterModeTypes, List<IOData> completeIOData,
				IOData lastIoData, Long lastTimestamp, Long lastTimestamp2) {

			int smtId = rMachineDataSet.getIoData().getSupplyMode();
			PpSupplyModeType smt = findSupplyModeTypeIfExist(supplyModeTypes, smtId);

			if (smt == null) {
				smt = supplyModeTypeRepo.saveAndFlush(PpSupplyModeType.builder().id(smtId).build());
				supplyModeTypes.add(smt);
			}

			int wmtId = rMachineDataSet.getIoData().getWasteMode();
			PpWasteModeType wmt = findWasteModeTypeIfExist(wasteModeTypes, wmtId);

			if (wmt == null) {
				wmt = wasteModeTypeRepo.saveAndFlush(PpWasteModeType.builder().id(wmtId).build());
				wasteModeTypes.add(wmt);
			}

			int gmtId = rMachineDataSet.getIoData().getGpMode();
			GpModeType gmt = findGpModeTypeIfExist(gpModeTypes, gmtId);

			if (gmt == null) {
				gmt = gpModeTypeRepo.saveAndFlush(GpModeType.builder().id(gmtId).build());
				gpModeTypes.add(gmt);
			}

			int hmtId = rMachineDataSet.getIoData().getHeaterMode();
			HeaterModeType hmt = findHeaterModeTypeIfExist(heaterModeTypes, hmtId);

			if (hmt == null) {
				hmt = heaterModeTypeRepo.saveAndFlush(HeaterModeType.builder().id(hmtId).build());
				heaterModeTypes.add(hmt);
			}

			IOData ioData = IOData.builder().supplyMode(smt).wasteMode(wmt).gpMode(gmt).heaterMode(hmt).build();

			if (lastIoData.getGpMode() == null && lastIoData.getHeaterMode() == null
					&& lastIoData.getSupplyMode() == null && lastIoData.getWasteMode() == null) {
				ioData.setStart(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
						rMachineDataSet.getIoData().getTimestamp2()));
				lastIoData = ioData;
			}

			if (!lastIoData.getSupplyMode().equals(smt) && !lastIoData.getWasteMode().equals(wmt)
					&& !lastIoData.getGpMode().equals(gmt) && !lastIoData.getHeaterMode().equals(hmt)) {
				lastIoData.setEnd(timestampConverter.convert(lastTimestamp, lastTimestamp2));
				lastIoData.setMachineRun(machineRun);
				completeIOData.add(lastIoData);
				ioData.setStart(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
						rMachineDataSet.getIoData().getTimestamp2()));
				lastIoData = ioData;
			}

			return lastIoData;
		}

		/**
		 * Adds the new errors.
		 *
		 * @param lastActiveErrors                the last active errors
		 * @param currentErrorElements            the current active errors
		 * @param lastActiveErrorsTimestampsStart the last active errors timestamps
		 *                                        start
		 * @return the list of active errors
		 */
		private List<ActiveError> addNewErrors(List<ActiveError> lastActiveErrors,
				List<RErrorElement> currentErrorElements, List<Severity> severities, List<Error> errors) {

			List<Integer> lastIds = new ArrayList<>();
			for (ActiveError activeError : lastActiveErrors) {
				lastIds.add(activeError.getError().getId());
			}

			for (RErrorElement rErrorElement : currentErrorElements) {

				if (!lastIds.contains(rErrorElement.getErrorId())) {
					int errorId = rErrorElement.getErrorId();
					Error error = findErrorIfExist(errors, errorId);

					if (error == null) {
						error = errorRepo.saveAndFlush(Error.builder().Id(errorId).build());
						errors.add(error);
					}

					int severityId = rErrorElement.getErrorLevel();
					Severity severity = findErrorLevelIfExist(severities, severityId);

					if (severity == null) {
						severity = severityRepo
								.saveAndFlush(Severity.builder().id(severityId).build());
						severities.add(severity);
					}

					ActiveError errorX = ActiveError.builder().error(error).severity(severity)
							.start(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
									rMachineDataSet.getIoData().getTimestamp2()))
							.build();
					lastActiveErrors.add(errorX);
				}
			}

			return lastActiveErrors;
		}

		/**
		 * Handle canceled errors.
		 *
		 * @param completeActiveErrors            the complete active errors
		 * @param lastActiveErrors                the last active errors
		 * @param currentErrorElements            the current active errors
		 * @param lastActiveErrorsTimestampsStart the last active errors timestamps
		 *                                        start
		 * @param lastTimestamp                   the last timestamp
		 * @param errorLevels                     the error levels
		 * @param errors                          the errors
		 * @return the list of active errors
		 */
		private List<ActiveError> handleCanceledErrors(List<ActiveError> completeActiveErrors,
				List<ActiveError> lastActiveErrors, List<RErrorElement> currentErrorElements, Long lastTimestamp,
				Long lastTimestamp2) {

			List<Integer> currentIds = new ArrayList<>();
			for (RErrorElement currentError : currentErrorElements) {
				currentIds.add(currentError.getErrorId());
			}

			List<ActiveError> deltaActiveErrors = new ArrayList<>();
			deltaActiveErrors.addAll(lastActiveErrors);

			for (ActiveError activeError : deltaActiveErrors) {
				if (!currentIds.contains(activeError.getError().getId())) {
					lastActiveErrors.remove(activeError);
					activeError.setEnd(timestampConverter.convert(lastTimestamp, lastTimestamp2));
					activeError.setMachineRun(machineRun);
					completeActiveErrors.add(activeError);
				}
			}

			return lastActiveErrors;
		}

		/**
		 * Sets the system states.
		 *
		 * @param completeSystemStates          the complete system states
		 * @param lastSystemStates              the last system states
		 * @param lastSystemStateTimestampStart the last system state timestamp start
		 * @param lastSystemStateTimestampEnd   the last system state timestamp end
		 * @param lastTimestamp                 the last timestamp
		 * @return the list of system states
		 */
		private List<RunningSystemState> setSystemStates(List<RunningSystemState> completeSystemStates,
				RunningSystemState lastSystemState) {

			lastSystemState.setEnd(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
					rMachineDataSet.getIoData().getTimestamp2()));
			lastSystemState.setMachineRun(machineRun);
			completeSystemStates.add(lastSystemState);

			return completeSystemStates;
		}

		/**
		 * Sets the protect states.
		 *
		 * @param completeProtectStates          the complete protect states
		 * @param lastProtectStates              the last protect states
		 * @param lastProtectStateTimestampStart the last protect state timestamp start
		 * @param lastProtectStateTimestampEnd   the last protect state timestamp end
		 * @param lastTimestamp                  the last timestamp
		 * @return
		 */
		private List<ProtectState> setProtectStates(List<ProtectState> completeProtectStates,
				ProtectState lastProtectStates) {

			lastProtectStates.setEnd(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
					rMachineDataSet.getIoData().getTimestamp2()));
			lastProtectStates.setMachineRun(machineRun);
			completeProtectStates.add(lastProtectStates);

			return completeProtectStates;
		}

		/**
		 * Sets the ps status.
		 *
		 * @param completePsStatus           the complete ps status
		 * @param lastPsStatus               the last ps status
		 * @param lastPsStatusTimestampStart the last ps status timestamp start
		 * @param lastPsStatusTimestampEnd   the last ps status timestamp end
		 * @param lastTimestamp              the last timestamp
		 * @return
		 */
		private List<PSStatus> setPsStatus(List<PSStatus> completePsStatus, PSStatus lastPsStatus) {

			lastPsStatus.setEnd(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
					rMachineDataSet.getIoData().getTimestamp2()));
			lastPsStatus.setMachineRun(machineRun);
			completePsStatus.add(lastPsStatus);

			return completePsStatus;
		}

		/**
		 * Sets the missed messages.
		 *
		 * @param completePsMissedMessage           the complete ps missed message
		 * @param lastPsMissedMessage               the last ps missed message
		 * @param lastPsMissedMessageTimestampStart the last ps missed message timestamp
		 *                                          start
		 * @param lastPsMissedMessageTimestampEnd   the last ps missed message timestamp
		 *                                          end
		 * @param lastTimestamp                     the last timestamp
		 * @return
		 */
		private List<PSMissedMessage> setMissedMessages(List<PSMissedMessage> completePsMissedMessage,
				PSMissedMessage lastPsMissedMessage) {

			lastPsMissedMessage.setEnd(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
					rMachineDataSet.getIoData().getTimestamp2()));
			lastPsMissedMessage.setMachineRun(machineRun);
			completePsMissedMessage.add(lastPsMissedMessage);

			return completePsMissedMessage;
		}

		/**
		 * Sets the io data.
		 *
		 * @param completeIoData           the complete io data
		 * @param lastIoData               the last io data
		 * @param lastIoDataTimestampStart the last io data timestamp start
		 * @param lastIoDataTimestampEnd   the last io data timestamp end
		 * @param lastTimestamp            the last timestamp
		 * @return
		 */
		private List<IOData> setIoData(List<IOData> completeIoData, IOData lastIoData, Timestamp end) {

			lastIoData.setEnd(end);
			lastIoData.setMachineRun(machineRun);
			completeIoData.add(lastIoData);
			return completeIoData;
		}

		/**
		 * Close all remaining errors.
		 *
		 * @param completeActiveErrors            the complete active errors
		 * @param lastActiveErrors                the last active errors
		 * @param currentErrorElements            the current active errors
		 * @param lastActiveErrorsTimestampsStart the last active errors timestamps
		 *                                        start
		 * @param end                             the current time stamp
		 * @return
		 */
		private List<ActiveError> closeAllRemainingErrors(List<ActiveError> completeActiveErrors,
				List<ActiveError> lastActiveErrors, Timestamp end) {

			for (ActiveError activeError : lastActiveErrors) {
				activeError.setEnd(end);
				activeError.setMachineRun(machineRun);
				completeActiveErrors.add(activeError);
			}
			return completeActiveErrors;
		}

		/**
		 * Gets the active errors only.
		 *
		 * @param currentErrorElements the current active errors
		 * @return the active errors only
		 */
		private List<RErrorElement> getActiveErrorsOnly(List<RErrorElement> currentErrorElements) {
			List<RErrorElement> errorElements;
			errorElements = rMachineDataSet.getErrorData().getErrorElements();
			int size = errorElements.size();
			// get only active errors
			for (int i = 0; i < size; i++) {
				if (errorElements.get(i).getErrorId() == 0) {
					currentErrorElements = errorElements.subList(0, i == 0 ? 0 : i);
					break;
				}
			}
			return currentErrorElements;
		}

		private RunningSystemState handleSystemStates(RunningSystemState lastSystemState,
				List<RunningSystemState> completeSystemStates, Long lastTimestamp, Long lastTimestamp2) {

			int mainState = rMachineDataSet.getProcessControlData().getSystemState();
			int stateValue = this.getStateValue(mainState);
			int activeSubState = rMachineDataSet.getProcessControlData().getActiveSubsequence();
			int subState = rMachineDataSet.getProcessControlData().getSubsequenceState();

			RunningSystemState state = RunningSystemState.builder().systemStateNode(systemState[mainState][stateValue])
					.build();

			String systemStateNode = state.getSystemStateNode().getNodeString();

			if (activeSubState > 0) {
				state.setSubsequenceNode(systemStateWithSubstate[activeSubState][subState]);
				systemStateNode = systemStateNode.concat(state.getSubsequenceNode().getNodeString());
			}

			if (lastSystemState.getSystemStateNode() == null) {
				state.setStart(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
						rMachineDataSet.getIoData().getTimestamp2()));
				lastSystemState = state;
			}

			String lastNodeString = lastSystemState.getSystemStateNode().getNodeString();

			if (lastSystemState.getSubsequenceNode() != null) {
				String lastSequenceString = lastSystemState.getSubsequenceNode().getNodeString();
				lastNodeString = lastNodeString.concat(lastSequenceString);
			}

			if (!lastNodeString.equalsIgnoreCase(systemStateNode)) {
				lastSystemState.setEnd(timestampConverter.convert(lastTimestamp, lastTimestamp2));
				lastSystemState.setMachineRun(machineRun);
				completeSystemStates.add(lastSystemState);
				state.setStart(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
						rMachineDataSet.getIoData().getTimestamp2()));
				lastSystemState = state;
			}

			return lastSystemState;
		}

		/**
		 * Handle protect states.
		 *
		 * @param lastProtectStates              the last protect states
		 * @param lastProtectStateTimestampStart the last protect state timestamp start
		 * @param lastProtectStateTimestampEnd   the last protect state timestamp end
		 * @param lastTimestamp                  the last timestamp
		 * @return the list of protect states
		 */
		private ProtectState handleProtectStates(ProtectState lastProtectStates,
				List<ProtectState> completeProtectStates, Long lastTimestamp, Long lastTimestamp2) {
			ProtectState protectState = ProtectState.builder()
					.protectState(rMachineDataSet.getProtectiveSystemData().getProtectState())
					.protectSubstate(rMachineDataSet.getProtectiveSystemData().getProtectSubstate()).build();

			if (lastProtectStates.getStart() == null) {
				protectState.setStart(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
						rMachineDataSet.getIoData().getTimestamp2()));
				lastProtectStates = protectState;
			}

			if (lastProtectStates.getProtectState() != protectState.getProtectState()
					&& lastProtectStates.getProtectSubstate() != protectState.getProtectSubstate()) {
				lastProtectStates.setEnd(timestampConverter.convert(lastTimestamp, lastTimestamp2));
				lastProtectStates.setMachineRun(machineRun);
				completeProtectStates.add(lastProtectStates);
				protectState.setStart(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
						rMachineDataSet.getIoData().getTimestamp2()));
				lastProtectStates = protectState;
			}

			return lastProtectStates;
		}

		/**
		 * Handle ps status.
		 *
		 * @param lastPsStatus               the last ps status
		 * @param lastPsStatusTimestampStart the last ps status timestamp start
		 * @param lastPsStatusTimestampEnd   the last ps status timestamp end
		 * @param lastTimestamp              the last timestamp
		 * @return the list of protective system status
		 */
		private PSStatus handlePsStatus(PSStatus lastPsStatus, List<PSStatus> completePsStatus, Long lastTimestamp,
				Long lastTimestamp2) {
			PSStatus psStatus = PSStatus.builder()
					.controlStatusByte(rMachineDataSet.getProtectiveSystemData().getControlStatusByte())
					.protectStatusByte(rMachineDataSet.getProtectiveSystemData().getProtectStatusByte()).build();

			if (lastPsStatus.getStart() == null) {
				psStatus.setStart(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
						rMachineDataSet.getIoData().getTimestamp2()));
				lastPsStatus = psStatus;
			}

			if (lastPsStatus.getControlStatusByte() != psStatus.getControlStatusByte()
					&& lastPsStatus.getProtectStatusByte() != psStatus.getProtectStatusByte()) {
				lastPsStatus.setEnd(timestampConverter.convert(lastTimestamp, lastTimestamp2));
				lastPsStatus.setMachineRun(machineRun);
				completePsStatus.add(lastPsStatus);
				psStatus.setStart(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
						rMachineDataSet.getIoData().getTimestamp2()));
				lastPsStatus = psStatus;
			}
			return lastPsStatus;
		}

		/**
		 * Handle missed messages.
		 * 
		 * @param allMissedMessages
		 *
		 * @param allMissedMessages               the last missed message
		 * @param lastMissedMessageTimestampStart the last missed message timestamp
		 *                                        start
		 * @param lastMissedMessageTimestampEnd   the last missed message timestamp end
		 * @param lastTimestamp                   the last timestamp
		 * @return the list of protective system missed messages
		 */
		private PSMissedMessage handleMissedMessages(PSMissedMessage lastMissedMessage,
				List<PSMissedMessage> allMissedMessages, Long lastTimestamp, Long lastTimestamp2) {
			PSMissedMessage missedMessage = PSMissedMessage.builder()
					.messageCounterError(rMachineDataSet.getProtectiveSystemData().getMessageCounterErrorCount())
					.sendMessageCounter(rMachineDataSet.getProtectiveSystemData().getSendMsgCounter()).build();

			if (lastMissedMessage.getStart() == null) {
				missedMessage.setStart(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
						rMachineDataSet.getIoData().getTimestamp2()));
				lastMissedMessage = missedMessage;
			}

			if (lastMissedMessage.getMessageCounterError() != missedMessage.getMessageCounterError()
					&& lastMissedMessage.getSendMessageCounter() != missedMessage.getSendMessageCounter()) {
				lastMissedMessage.setEnd(timestampConverter.convert(lastTimestamp, lastTimestamp2));
				lastMissedMessage.setMachineRun(machineRun);
				allMissedMessages.add(lastMissedMessage);
				missedMessage.setStart(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
						rMachineDataSet.getIoData().getTimestamp2()));
				lastMissedMessage = missedMessage;
			}

			return lastMissedMessage;
		}

		/**
		 * Find heater mode type if exist.
		 *
		 * @param heaterModeTypes the heater mode types
		 * @param hmtId           the heater mode type id
		 * @return the heater mode type
		 */
		private HeaterModeType findHeaterModeTypeIfExist(List<HeaterModeType> heaterModeTypes, int hmtId) {
			HeaterModeType hmt = Iterables.find(heaterModeTypes, new Predicate<HeaterModeType>() {

				@Override
				public boolean apply(HeaterModeType input) {
					if (input.getId() == hmtId) {
						return true;
					}
					return false;
				}
			}, null);
			return hmt;
		}

		/**
		 * Find gp mode type if exist.
		 *
		 * @param gpModeTypes the gp mode types
		 * @param gmtId       the gp mode type id
		 * @return the gp mode type
		 */
		private GpModeType findGpModeTypeIfExist(List<GpModeType> gpModeTypes, int gmtId) {
			GpModeType gmt = Iterables.find(gpModeTypes, new Predicate<GpModeType>() {

				@Override
				public boolean apply(GpModeType input) {
					if (input.getId() == gmtId) {
						return true;
					}
					return false;
				}
			}, null);
			return gmt;
		}

		/**
		 * Find waste mode type if exist.
		 *
		 * @param wasteModeTypes the waste mode types
		 * @param wmtId          the waste mode type id
		 * @return the pp waste mode type
		 */
		private PpWasteModeType findWasteModeTypeIfExist(List<PpWasteModeType> wasteModeTypes, int wmtId) {
			PpWasteModeType wmt = Iterables.find(wasteModeTypes, new Predicate<PpWasteModeType>() {

				@Override
				public boolean apply(PpWasteModeType input) {
					if (input.getId() == wmtId) {
						return true;
					}
					return false;
				}
			}, null);
			return wmt;
		}

		/**
		 * Find supply mode type if exist.
		 *
		 * @param supplyModeTypes the supply mode types
		 * @param smtId           the supply mode type id
		 * @return the pp supply mode type
		 */
		private PpSupplyModeType findSupplyModeTypeIfExist(List<PpSupplyModeType> supplyModeTypes, int smtId) {
			PpSupplyModeType smt = Iterables.find(supplyModeTypes, new Predicate<PpSupplyModeType>() {

				@Override
				public boolean apply(PpSupplyModeType input) {
					if (input.getId() == smtId) {
						return true;
					}
					return false;
				}
			}, null);
			return smt;
		}

		/**
		 * Find error level if exist.
		 *
		 * @param severities  the error levels
		 * @param severityId the error level id
		 * @return the error level
		 */
		private Severity findErrorLevelIfExist(List<Severity> severities, int severityId) {
			Severity severity = Iterables.find(severities, new Predicate<Severity>() {

				@Override
				public boolean apply(Severity input) {
					if (input.getId() == severityId) {
						return true;
					}
					return false;
				}
			}, null);
			return severity;
		}

		/**
		 * Find error if exist.
		 *
		 * @param errors  the errors
		 * @param errorId the error id
		 * @return the error
		 */
		private Error findErrorIfExist(List<Error> errors, int errorId) {
			Error error = Iterables.find(errors, new Predicate<Error>() {

				@Override
				public boolean apply(Error input) {
					if (input.getId() == errorId) {
						return true;
					}
					return false;
				}
			}, null);
			return error;
		}

		/**
		 * Handle digital outputs.
		 *
		 * @param digitalOutputTypes the digital output types
		 * @param tempDigOutputs     the temp dig outputs
		 * @return the SQL server data table
		 */
		private SQLServerDataTable handleDigitalOutputs(List<DigitalOutputType> digitalOutputTypes,
				SQLServerDataTable tempDigOutputs) {
			List<DigitalOutput> digitalOutputs = new ArrayList<>();
			digitalOutputList = rMachineDataSet.getDigitalOutput();
			for (RDigitalOutput digitalOutput : digitalOutputList) {

				int piId = digitalOutput.getDigitalOutputs();
				DigitalOutputType type = Iterables.find(digitalOutputTypes, new Predicate<DigitalOutputType>() {

					@Override
					public boolean apply(DigitalOutputType input) {
						if (input.getId() == piId) {
							return true;
						}
						return false;
					}
				}, null);

				if (type == null) {
					type = digitalOutputTypeRepo.saveAndFlush(DigitalOutputType.builder().id(piId).build());
					digitalOutputTypes.add(type);
				}

				boolean hmi = digitalOutput.isHmi();
				boolean dem = digitalOutput.isDem();

				DigitalOutput digitalOutX = DigitalOutput.builder().machineRun(machineRun)
						.machineDataLoopTimestamp(
								timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
										rMachineDataSet.getIoData().getTimestamp2()))
						.type(type).hmi(hmi).dem(dem).build();
				digitalOutputs.add(digitalOutX);
			}

			digitalOutputs.forEach(item -> {
				try {
					tempDigOutputs.addRow(item.getMachineDataLoopTimestamp(), item.isDem(), item.isHmi(),
							item.getMachineRun().getID(), item.getType().getId());
				} catch (SQLServerException e) {
					e.printStackTrace();
				}
			});

			return tempDigOutputs;
		}

		/**
		 * Handle numeric outputs.
		 *
		 * @param numericOutputTypes the numeric output types
		 * @param tempNumOutputs     the temp num outputs
		 * @return the SQL server data table
		 */
		private SQLServerDataTable handleNumericOutputs(List<NumericOutputType> numericOutputTypes,
				SQLServerDataTable tempNumOutputs) {
			List<NumericOutput> numericOutputs = new ArrayList<>();
			numericOutputList = rMachineDataSet.getNumericOutput();
			for (RNumericOutput numericOutput : numericOutputList) {

				int piId = numericOutput.getNumericOutput();
				NumericOutputType type = Iterables.find(numericOutputTypes, new Predicate<NumericOutputType>() {

					@Override
					public boolean apply(NumericOutputType input) {
						if (input.getId() == piId) {
							return true;
						}
						return false;
					}
				}, null);

				if (type == null) {
					type = numericOutputTypeRepo.saveAndFlush(NumericOutputType.builder().id(piId).build());
					numericOutputTypes.add(type);
				}

				float dem = numericOutput.getDem();
				float cd = numericOutput.getCd();

				if (cd != 0 || dem != 0) {
					NumericOutput numericOutX = NumericOutput.builder()
							.machineDataLoopTimestamp(
									timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
											rMachineDataSet.getIoData().getTimestamp2()))
							.machineRun(machineRun).type(type).dem(dem).cd(cd).build();
					numericOutputs.add(numericOutX);
				}
			}

			numericOutputs.forEach(item -> {
				try {
					tempNumOutputs.addRow(item.getMachineDataLoopTimestamp(), item.getCd(), item.getDem(),
							item.getMachineRun().getID(), item.getType().getId());
				} catch (SQLServerException e) {
					e.printStackTrace();
				}
			});

			return tempNumOutputs;
		}

		/**
		 * Handle digital inputs.
		 *
		 * @param digitalInputTypes the digital input types
		 * @param tempDigInputs     the temp dig inputs
		 * @return the SQL server data table
		 */
		private SQLServerDataTable handleDigitalInputs(List<DigitalInputType> digitalInputTypes,
				SQLServerDataTable tempDigInputs) {
			List<DigitalInput> digitalInputs = new ArrayList<>();
			digitalInputList = rMachineDataSet.getDigitalInput();
			for (RDigitalInput digitalInput : digitalInputList) {

				int piId = digitalInput.getDigitalInput();
				DigitalInputType type = Iterables.find(digitalInputTypes, new Predicate<DigitalInputType>() {

					@Override
					public boolean apply(DigitalInputType input) {
						if (input.getId() == piId) {
							return true;
						}
						return false;
					}
				}, null);

				if (type == null) {
					type = digitalInputTypeRepo.saveAndFlush(DigitalInputType.builder().id(piId).build());
					digitalInputTypes.add(type);
				}

				boolean cd = digitalInput.isCd();
				boolean r = digitalInput.isR();

				DigitalInput digitalInX = DigitalInput.builder()

						.machineDataLoopTimestamp(
								timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
										rMachineDataSet.getIoData().getTimestamp2()))
						.machineRun(machineRun).type(type).cd(cd).r(r).build();
				digitalInputs.add(digitalInX);

			}

			digitalInputs.forEach(item -> {
				try {
					tempDigInputs.addRow(item.getMachineDataLoopTimestamp(), item.isCd(), item.isR(),
							item.getMachineRun().getID(), item.getType().getId());
				} catch (SQLServerException e) {
					e.printStackTrace();
				}
			});

			return tempDigInputs;
		}

		/**
		 * Handle numeric inputs.
		 *
		 * @param numericInputTypes the numeric input types
		 * @param tempNumInputs     the temp num inputs
		 * @return the SQL server data table
		 */
		private SQLServerDataTable handleNumericInputs(List<NumericInputType> numericInputTypes,
				SQLServerDataTable tempNumInputs) {
			List<NumericInput> numericInputs = new ArrayList<>();
			numericInputList = rMachineDataSet.getNumericInput();
			for (RNumericInput numericInput : numericInputList) {

				int piId = numericInput.getNumericInput();
				NumericInputType type = Iterables.find(numericInputTypes, new Predicate<NumericInputType>() {

					@Override
					public boolean apply(NumericInputType input) {
						if (input.getId() == piId) {
							return true;
						}
						return false;
					}
				}, null);

				if (type == null) {
					type = numericInputTypeRepo.saveAndFlush(NumericInputType.builder().id(piId).build());
					numericInputTypes.add(type);
				}

				float r = numericInput.getR();
				float f = numericInput.getF();
				float cd = numericInput.getCd();

				if (numInIds.contains(piId)) {
					
					if (r != 0 || f != 0 || cd != 0) {
						NumericInput numericInX = NumericInput.builder()
								.machineDataLoopTimestamp(
										timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
												rMachineDataSet.getIoData().getTimestamp2()))
								.machineRun(machineRun).type(type).r(r).f(f).cd(cd).build();
						numericInputs.add(numericInX);
					}
				}
				

			}

			numericInputs.forEach(item -> {

				try {
					tempNumInputs.addRow(item.getMachineDataLoopTimestamp(), item.getCd(), item.getF(), item.getR(),
							item.getMachineRun().getID(), item.getType().getId());
				} catch (SQLServerException e) {
					e.printStackTrace();
				}
			});

			return tempNumInputs;
		}

		/**
		 * Handle closed loops.
		 *
		 * @param closedLoopTypes the closed loop types
		 * @param tempClosedloops the temp closedloops
		 * @return the SQL server data table
		 */
		private SQLServerDataTable handleClosedLoops(List<ClosedLoopType> closedLoopTypes,
				SQLServerDataTable tempClosedloops) {
			List<ClosedLoop> closedLoops = new ArrayList<>();
			closedLoopList = rMachineDataSet.getClosedLoop();
			for (RClosedLoop closedLoop : closedLoopList) {

				int piId = closedLoop.getClosedLoop();
				ClosedLoopType type = Iterables.find(closedLoopTypes, new Predicate<ClosedLoopType>() {

					@Override
					public boolean apply(ClosedLoopType input) {
						if (input.getId() == piId) {
							return true;
						}
						return false;
					}
				}, null);

				if (type == null) {
					type = closedLoopTypeRepo.saveAndFlush(ClosedLoopType.builder().id(piId).build());
					closedLoopTypes.add(type);
				}

				float hmi = closedLoop.getHmi();
				float proc = closedLoop.getProc();
				float dem = closedLoop.getDem();
				float y = closedLoop.getY();
				float inp = closedLoop.getInp();
				float e = closedLoop.getE();

				if (clLoopIds.contains(piId)) {
					if (hmi != 0 || proc != 0 || dem != 0 || y != 0 || inp != 0 || e != 0) {

						ClosedLoop closedLoopX = ClosedLoop.builder().machineRunId(machineRun)
								.machineDataLoopTimestamp(
										timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
												rMachineDataSet.getIoData().getTimestamp2()))
								.type(type)

								.hmi(hmi).proc(proc).dem(dem).y(y).inp(inp).e(e).build();
						closedLoops.add(closedLoopX);
					}
				}
			}

			closedLoops.forEach(item -> {
				try {
					tempClosedloops.addRow(item.getMachineDataLoopTimestamp(), item.getDem(), item.getE(),
							item.getHmi(), item.getInp(), item.getProc(), item.getY(), item.getMachineRunId().getID(),
							item.getType().getId());
				} catch (SQLServerException e1) {
					e1.printStackTrace();
				}
			});

			return tempClosedloops;
		}

		/**
		 * Sets the machine run ps config.
		 */
		private void setMachineRunPsConfig() {
			
				PSConfiguration psc =  PSConfiguration.builder()
						.acidDensity(rMachineDataSet.getProtectiveSystemData().getBalancingData().getAcidDensity())
						.baseDensity(rMachineDataSet.getProtectiveSystemData().getBalancingData().getBaseDensity())
						.machineRun(machineRun).build();
				psConfigRepo.save(psc);
			
		}

		/**
		 * Sets the machine run start.
		 */
		private void setMachineRunStart() {
			if (machineRun.getStart() == null) {
				machineRun.setStart(timestampConverter.convert(rMachineDataSet.getIoData().getTimestamp1(),
						rMachineDataSet.getIoData().getTimestamp2()));
				machineRun = machineRunRepo.save(machineRun);
			}
		}

		/**
		 * Gets the main state value.
		 *
		 * @param mainState the main state
		 * @return the main state value
		 */
		private int getStateValue(int mainState) {

			int stateValue = 0;
			switch (mainState) {
			case 0:
				stateValue = rMachineDataSet.getProcessControlData().getStartupState();
				break;
			case 1:
				stateValue = rMachineDataSet.getProcessControlData().getIdleState();
				break;
			case 2:
				stateValue = rMachineDataSet.getProcessControlData().getServiceState();
				break;
			case 3:
				stateValue = rMachineDataSet.getProcessControlData().getPreparationState();
				break;
			case 4:
				stateValue = rMachineDataSet.getProcessControlData().getTreatmentState();
				break;
			case 5:
				stateValue = rMachineDataSet.getProcessControlData().getPostprocessState();
				break;
			case 6:
				stateValue = rMachineDataSet.getProcessControlData().getDesinfectionState();
				break;
			default:
				break;
			}

			return stateValue;
		}

		/**
		 * Match file version.
		 *
		 * @param fileName the file name
		 * @return true, if successful
		 */
		private boolean matchFileVersion(String fileName) {

			String version = fileName.substring(38);
			String regex1 = "([3]{1}|[4-9]{1}|[0-9]{2,})\\.([7-9]{1}|[0-9]{2,})\\.\\d+_.*";
			String regex2 = "3\\.6\\.\\d+_t9([0-9][4-9]|[1-9][0-9]).*";

			if (version.matches(regex1)) {
				return true;
			}
			if (version.matches(regex2)) {
				return true;
			}

			return false;
		}
		
		private boolean matchVersion(String fileName) {

			String[] a = fileName.split("_");
			String b = a[7];
			String version = b.substring(7);
			String regex = "([3][.]([9]|[0-9]{2,})[.]\\d+?)|([4-9][.]\\d+?[.]\\d+?)";
			
			log.info(version);

			if (version.matches(regex)) {
				return true;
			}
			
			return false;
		}
	}

}
