/**
 * 
 */
package com.hw.importservice.filereader;

import java.io.IOException;
import java.math.BigDecimal;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.hw.importservice.ImportServiceInMemoryTables;
import com.hw.parser.beans.RHeaderData;
import com.hw.parser.beans.RMachineDataSet;
import com.hw.parser.beans.headerdata.RCalibrationData;
import com.hw.parser.beans.headerdata.RFileHeader;
import com.hw.parser.beans.headerdata.RSystemConfiguration;
import com.hw.parser.beans.machinedata.RAnalogIn;
import com.hw.parser.beans.machinedata.RBalancingData;
import com.hw.parser.beans.machinedata.RClosedLoop;
import com.hw.parser.beans.machinedata.RDigitalInput;
import com.hw.parser.beans.machinedata.RDigitalOutput;
import com.hw.parser.beans.machinedata.RErrorData;
import com.hw.parser.beans.machinedata.RErrorElement;
import com.hw.parser.beans.machinedata.RIOData;
import com.hw.parser.beans.machinedata.RNumericInput;
import com.hw.parser.beans.machinedata.RNumericOutput;
import com.hw.parser.beans.machinedata.RProcessControlData;
import com.hw.parser.beans.machinedata.RProtectiveSystemData;
import com.hw.parser.beans.machinedata.RPumpData;
import com.hw.parser.beans.machinedata.RSondData;
import com.hw.parser.beans.machinedata.RSystemDiagnosis;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * Read HWBIN binary files that contain the machine data.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Data
public class HwBinFileReader {

	
	private HwBinConverter hwBinConverter;

	/**
	 * Creates a new {@link HwBinFileReader} object that allocate a newly
	 * created {@link HwBinConverter} object.
	 */
	public HwBinFileReader() {
		super();
		this.hwBinConverter = new HwBinConverter();
	}

	/**
	 * Returns an instance of {@link RHeaderData} that represent the values of
	 * this {@code ByteBuffer}.
	 * <p>
	 * This method read {@link RHeaderData} data from this current
	 * {@code ByteBuffer} using the methods: <blockquote>
	 * 
	 * <pre>
	 * {@link #readFileHeader(ByteBuffer)}
	 * {@link #readSystemConfiguration(ByteBuffer)}
	 * {@link #readCalibrationData(ByteBuffer)}
	 * </pre>
	 * 
	 * </blockquote>
	 * 
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * 
	 * @return An instance of {@link RHeaderData}
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	public RHeaderData readHeaderData(ByteBuffer bb) throws IOException {

		return RHeaderData
				.builder()
				.fileHeader(this.readFileHeader(bb))
				.systemConfiguration(this.readSystemConfiguration(bb))
				.calibrationData(this.readCalibrationData(bb))
				.build();

	}

	/**
	 * Returns an instance of {@link RMachineDataSet} that represent the values
	 * of this {@code ByteBuffer}.
	 * <p>
	 * This method read one {@link RMachineDataSet} from this current
	 * {@code ByteBuffer} using the methods: <blockquote>
	 * 
	 * <pre>
	 * {@link #readNumericInputs(ByteBuffer)}
	 * {@link #readDigitalInputs(ByteBuffer)}
	 * {@link #readClosedLoops(ByteBuffer)}
	 * {@link #readNumericOutputs(ByteBuffer)}
	 * {@link #readDigitalOutputs(ByteBuffer)}
	 * {@link #readIoData(ByteBuffer)}
	 * {@link #readProcessControlData(ByteBuffer)}
	 * {@link #readProtectiveSystemData(ByteBuffer)}
	 * {@link #readSystemDiagnosis(ByteBuffer)}
	 * {@link #readErrorData(ByteBuffer)}
	 * </pre>
	 * 
	 * </blockquote>
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An instance of {@link RMachineDataSet}
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	public RMachineDataSet readMachineDataSet(ByteBuffer bb, boolean b) throws IOException {

		List<RNumericInput> numericInputs = readNumericInputs(bb);
		List<RDigitalInput> digitalInputs = readDigitalInputs(bb);
		List<RClosedLoop> closedLoops = readClosedLoops(bb);
		List<RNumericOutput> numericOutputs = readNumericOutputs(bb);
		List<RDigitalOutput> digitalOutputs = readDigitalOutputs(bb);
		RIOData ioData = readIoData(bb);
		RProcessControlData processControlData = readProcessControlData(bb);
		RProtectiveSystemData protectiveSystemData = readProtectiveSystemData(bb, b);
		RSystemDiagnosis systemDiagnosis = readSystemDiagnosis(bb);
		RErrorData errorData = readErrorData(bb);

		return RMachineDataSet
				.builder()
				.numericInput(numericInputs)
				.digitalInput(digitalInputs)
				.closedLoop(closedLoops)
				.numericOutput(numericOutputs)
				.digitalOutput(digitalOutputs)
				.ioData(ioData)
				.processControlData(processControlData)
				.protectiveSystemData(protectiveSystemData)
				.systemDiagnosis(systemDiagnosis)
				.errorData(errorData)
				.build();

	}

	/**
	 * Returns an instance of {@link RFileHeader} that represent the values of
	 * this {@code ByteBuffer}.
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An instance of {@link RFileHeader}
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private RFileHeader readFileHeader(ByteBuffer bb) throws IOException {

		return RFileHeader
				.builder()
				.fileIdentifier(hwBinConverter.readString(bb, 2))
				.sectionSize(hwBinConverter.readInt(bb))
				.offset(hwBinConverter.readInt(bb))
				.fileFormatVersion(new BigDecimal(hwBinConverter.readInt(bb)).movePointLeft(2))
				.build();

	}

	/**
	 * Returns an instance of {@link RSystemConfiguration} that represent the
	 * values of this {@code ByteBuffer}.
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An instance of {@link RSystemConfiguration}
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private RSystemConfiguration readSystemConfiguration(ByteBuffer bb) throws IOException {

		bb.position(16);

		return RSystemConfiguration
				.builder()
				.sectionSize(hwBinConverter.readInt(bb))
				.machineNumber(hwBinConverter.readString(bb, 4))
				.hostVersion(hwBinConverter.readString(bb, 24))
				.controlVersion(hwBinConverter.readString(bb, 24))
				.protectVersion(hwBinConverter.readString(bb, 24))
				.parameterFile(hwBinConverter.readString(bb, 128))
				.clinicName(hwBinConverter.readString(bb, 128))
				.build();

	}

	/**
	 * Returns an instance of {@link RCalibrationData} that represent the values
	 * of this {@code ByteBuffer}.
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An instance of {@link RCalibrationData}
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private RCalibrationData readCalibrationData(ByteBuffer bb) throws IOException {

		List<Double> calibration = new LinkedList<>();
		
		bb.position(512);
		int sectionSize = hwBinConverter.readInt(bb);
		int numberOfEntries = hwBinConverter.readInt(bb);
		
		for(int i = 0; i < numberOfEntries; i++) {
			calibration.add(hwBinConverter.readDouble(bb));
		}
		
		RCalibrationData calibrationData = RCalibrationData
				.builder()
				.sectionSize(sectionSize)
				.numberOfEntries(numberOfEntries)
				.calibration(calibration).build();

		return calibrationData;
	}

	/**
	 * Returns an {@code ArrayList} of {@link RNumericInput}'s that represent the
	 * values of this {@code ByteBuffer}.
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An {@code ArrayList} of {@link RNumericInput}'s
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private List<RNumericInput> readNumericInputs(ByteBuffer bb) throws IOException {

		List<RNumericInput> numericInputs = new ArrayList<>();
		int size = hwBinConverter.readInt(bb);
		// log.info("numeric inputs: " + size);
		for (int i = 0; i < size; i++) {
			numericInputs.add(RNumericInput
					.builder()
					
					.numericInput(hwBinConverter.readInt(bb))
					.r(hwBinConverter.readFloat(bb))
					.f(hwBinConverter.readFloat(bb))
					.cd(hwBinConverter.readFloat(bb))
					.build());
		}

		return numericInputs;

	}

	/**
	 * Returns an {@code ArrayList} of {@link RDigitalInput}'s that represent the
	 * values of this {@code ByteBuffer}.
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An {@code ArrayList} of {@link RDigitalInput}'s
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private List<RDigitalInput> readDigitalInputs(ByteBuffer bb) throws IOException {

		List<RDigitalInput> digitalInputs = new ArrayList<>();
		int size = hwBinConverter.readInt(bb);
		// log.info("digital inputs: " + size);
		for (int i = 0; i < size; i++) {

			digitalInputs.add(RDigitalInput
					.builder()
					
					.digitalInput(hwBinConverter.readInt(bb))
					.cd(hwBinConverter.readBoolean(bb))
					.r(hwBinConverter.readBoolean(bb))
					.build());
		}

		return digitalInputs;
	}

	/**
	 * Returns an {@code ArrayList} of {@link RClosedLoop}'s that represent the
	 * values of this {@code ByteBuffer}.
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An {@code ArrayList} of {@link RClosedLoop}'s
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private List<RClosedLoop> readClosedLoops(ByteBuffer bb) throws IOException {

		List<RClosedLoop> closedLoops = new ArrayList<>();
		int size = hwBinConverter.readInt(bb);
		// log.info("closed loops: " + size);
		for (int i = 0; i < size; i++) {
			closedLoops.add(RClosedLoop
					.builder()
					
					.closedLoop(hwBinConverter.readInt(bb))
					.hmi(hwBinConverter.readFloat(bb))
					.proc(hwBinConverter.readFloat(bb))
					.dem(hwBinConverter.readFloat(bb))
					.y(hwBinConverter.readFloat(bb))
					.inp(hwBinConverter.readFloat(bb))
					.e(hwBinConverter.readFloat(bb))
					.build());
		}

		return closedLoops;
	}

	/**
	 * Returns an {@code ArrayList} of {@link RNumericOutput}'s that represent
	 * the values of this {@code ByteBuffer}.
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An {@code ArrayList} of {@link RNumericOutput}'s
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private List<RNumericOutput> readNumericOutputs(ByteBuffer bb) throws IOException {

		List<RNumericOutput> numericOutputs = new ArrayList<>();
		int size = hwBinConverter.readInt(bb);
		// log.info("numeric outputs: " + size);
		for (int i = 0; i < size; i++) {

			numericOutputs.add(RNumericOutput
					.builder()
					
					.numericOutput(hwBinConverter.readInt(bb))
					.dem(hwBinConverter.readFloat(bb))
					.cd(hwBinConverter.readFloat(bb))
					.build());
		}

		return numericOutputs;
	}

	/**
	 * Returns an {@code ArrayList} of {@link RDigitalOutput}'s that represent
	 * the values of this {@code ByteBuffer}.
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An {@code ArrayList} of {@link RDigitalOutput}'s
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private List<RDigitalOutput> readDigitalOutputs(ByteBuffer bb) throws IOException {

		List<RDigitalOutput> digitalOutputs = new ArrayList<>();
		int size = hwBinConverter.readInt(bb);
		// log.info("digital outputs: " + size);
		for (int i = 0; i < size; i++) {
			digitalOutputs.add(RDigitalOutput
					.builder()
					
					.digitalOutputs(hwBinConverter.readInt(bb))
					.hmi(hwBinConverter.readBoolean(bb))
					.dem(hwBinConverter.readBoolean(bb))
					.build());
		}

		return digitalOutputs;
	}

	/**
	 * Returns an instance of {@link RIOData} that represent the values of this
	 * {@code ByteBuffer}.
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An instance of {@link RIOData}
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private RIOData readIoData(ByteBuffer bb) throws IOException {

		return RIOData
				.builder()
				.timestamp1(hwBinConverter.readLong(bb))
				.timestamp2(hwBinConverter.readLong(bb))
				.supplyMode(hwBinConverter.readByte(bb))
				.wasteMode(hwBinConverter.readByte(bb))
				.gpMode(hwBinConverter.readByte(bb))
				.heaterMode(hwBinConverter.readByte(bb))
				.build();

	}

	/**
	 * Returns an instance of {@link RProcessControlData} that represent the
	 * values of this {@code ByteBuffer}.
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An instance of {@link RProcessControlData}
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private RProcessControlData readProcessControlData(ByteBuffer bb) throws IOException {

		return RProcessControlData
				.builder()
				.systemState(hwBinConverter.readByte(bb))
				.idleState(hwBinConverter.readByte(bb))
				.startupState(hwBinConverter.readByte(bb))
				.serviceState(hwBinConverter.readByte(bb))
				.preparationState(hwBinConverter.readByte(bb))
				.treatmentState(hwBinConverter.readByte(bb))
				.postprocessState(hwBinConverter.readByte(bb))
				.desinfectionState(hwBinConverter.readByte(bb))
				.activeSubsequence(hwBinConverter.readByte(bb))
				.subsequenceState(hwBinConverter.readByte(bb))
				.build();
	}

	/**
	 * Returns an instance of {@link RProtectiveSystemData} that represent the
	 * values of this {@code ByteBuffer}.
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An instance of {@link RProtectiveSystemData}
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private RProtectiveSystemData readProtectiveSystemData(ByteBuffer bb, boolean b) throws IOException {

		List<RSondData> sondData = new LinkedList<>();
		for (int i = 0; i < 4; i++) {
			sondData.add(RSondData
					.builder()
					.upH(hwBinConverter.readShort(bb))
					.uTemp(hwBinConverter.readShort(bb))
					.temperature(hwBinConverter.readFloat(bb))
					.ph(hwBinConverter.readFloat(bb))
					.build());

		}
		//log.info("/n sond data: "+sondData);

		List<RPumpData> pumpData = new LinkedList<>();
		for (int i = 0; i < 2; i++) {
			pumpData.add(RPumpData
					.builder()
					.host(hwBinConverter.readFloat(bb))
					.control(hwBinConverter.readFloat(bb))
					.meas(hwBinConverter.readFloat(bb))
					.build());

		}

		List<RAnalogIn> analogIn = new LinkedList<>();
		for (int i = 0; i < 10; i++) {
			analogIn.add(RAnalogIn
					.builder()
					.f(hwBinConverter.readFloat(bb))
					.cd(hwBinConverter.readFloat(bb))
					.build());

		}

		RBalancingData balancingData = RBalancingData
				.builder()
				.ufHost(hwBinConverter.readFloat(bb))
				.ufControl(hwBinConverter.readFloat(bb))
				.ufWeight(hwBinConverter.readFloat(bb))
				.acidFlowConrol(hwBinConverter.readFloat(bb))
				.acidWeight(hwBinConverter.readFloat(bb))
				.acidDensity(hwBinConverter.readFloat(bb))
				.baseFlowControl(hwBinConverter.readFloat(bb))
				.baseWeight(hwBinConverter.readFloat(bb))
				.baseDensity(hwBinConverter.readFloat(bb))
				.scale1row(hwBinConverter.readFloat(bb))
				.scale2row(hwBinConverter.readFloat(bb))
				.scaleDeviation(hwBinConverter.readFloat(bb))
				.scaleSumCurrent(hwBinConverter.readFloat(bb))
				.build();
						
		if (b) {
			balancingData.setBalanceOffset(hwBinConverter.readFloat(bb));
		} else {
			balancingData.setBalanceOffset(-5);
		}

		return RProtectiveSystemData
				.builder()
				.unknownMessageCount(hwBinConverter.readInt(bb))
				.crcErrorCount(hwBinConverter.readInt(bb))
				.messageCounterErrorCount(hwBinConverter.readInt(bb))
				.sendMsgCounter(hwBinConverter.readByte(bb))
				.protectState(hwBinConverter.readByte(bb))
				.protectSubstate(hwBinConverter.readByte(bb))
				.controlStatusByte(hwBinConverter.readByte(bb))
				.protectStatusByte(hwBinConverter.readByte(bb))
				.sondData(sondData)
				.pumpData(pumpData)
				.analogIn(analogIn)
				.lastReceivedMessageID(hwBinConverter.readByte(bb))
				.lastSendMessageID(hwBinConverter.readByte(bb))
				.lastSendMainState(hwBinConverter.readByte(bb))
				.lastSendSubState(hwBinConverter.readByte(bb))
				.rs232CommunicationLoopExecutionTime(hwBinConverter.readInt(bb))
				.balancingData(balancingData)
				.build();

	}

	/**
	 * Returns an instance of {@link RSystemDiagnosis} that represent the values
	 * of this {@code ByteBuffer}.
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An instance of {@link RSystemDiagnosis}
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private RSystemDiagnosis readSystemDiagnosis(ByteBuffer bb) throws IOException {

		return RSystemDiagnosis
				.builder()
				.processorLoad(hwBinConverter.readFloat(bb))
				.memoryUsage(hwBinConverter.readFloat(bb))
				.systemLoopProcessingTime(hwBinConverter.readFloat(bb))
				.loopLateCounter(hwBinConverter.readFloat(bb))
				.hostTotalProcessorLoad(hwBinConverter.readFloat(bb))
				.hostLK2001ProcessorLoad(hwBinConverter.readFloat(bb))
				.hostLK2001PrivateBytes(hwBinConverter.readFloat(bb))
				.hostReconnectCounter(hwBinConverter.readFloat(bb))
				.hostConnectionStageChangeCounter(hwBinConverter.readFloat(bb))
				.durationLastSavingProcess(hwBinConverter.readFloat(bb))
				.open(hwBinConverter.readFloat(bb))
				.setPointerToEnd(hwBinConverter.readFloat(bb))
				.sdwrite(hwBinConverter.readFloat(bb))
				.sdflush(hwBinConverter.readFloat(bb))
				.sdclose(hwBinConverter.readFloat(bb))
				.elementInQ(hwBinConverter.readFloat(bb))
				.build();

	}

	/**
	 * Returns an instance of {@link RErrorData} that represent the values of
	 * this {@code ByteBuffer}.
	 * 
	 * @param bb
	 *            - The current {@code ByteBuffer}
	 * @return An instance of {@link RErrorData}
	 * @throws IOException
	 *             If an I/O error occurs
	 */
	private RErrorData readErrorData(ByteBuffer bb) throws IOException {

		List<RErrorElement> errorElements = new ArrayList<>();
		int size = hwBinConverter.readInt(bb);		
		for (int i = 0; i < size; i++) {
			errorElements.add(RErrorElement
					.builder()
					.errorId(hwBinConverter.readInt(bb))
					.errorLevel(hwBinConverter.readInt(bb))
					.build());

		}

		float phReservoirRangeCheck = hwBinConverter.readFloat(bb);

		return RErrorData
				.builder()
				.errorElements(errorElements)
				.phReservoirRangeCheck(phReservoirRangeCheck)
				.build();

	}

	/**
	 * Convert {@code byte} values to other primitive data types or to a
	 * {@code String}.
	 * <p>
	 * This class provide methods for converting {@code byte} values to
	 * {@code int}, {@code long}, {@code short}, {@code float}, {@code double},
	 * {@code boolean} or {@code String} from a given {@code ByteBuffer}.
	 * 
	 * @author kaja
	 * @version 1.0
	 *
	 */
	public class HwBinConverter {

		/**
		 * Returns a {@code int} value of this {@code ByteBuffer}.
		 * 
		 *
		 * @param bb
		 *            - The current {@code ByteBuffer}
		 * @return The {@code int} value of this {@code ByteBuffer}
		 */
		public int readInt(ByteBuffer bb) {

			int i = bb.getInt();
			return i;
		}

		/**
		 * Returns a {@code float} value of this {@code ByteBuffer}.
		 * 
		 * @param bb
		 *            - The current {@code ByteBuffer}
		 * @return The {@code float} value of this {@code ByteBuffer}
		 */
		public float readFloat(ByteBuffer bb) {

			float f = bb.getFloat();
			if(Float.isNaN(f)) {
				f=0;
			}
			return f;
		}

		/**
		 * Returns a {@code double} value of this {@code ByteBuffer}.
		 * 
		 * @param bb
		 *            - The current {@code ByteBuffer}
		 * @return The {@code double} value of this {@code ByteBuffer}
		 */
		public double readDouble(ByteBuffer bb) {

			double d = bb.getDouble();
			d = Double.isNaN(d)? 0 : d;
			return d;
		}

		/**
		 * Returns a {@code long} value of this {@code ByteBuffer}.
		 * 
		 * @param bb
		 *            - The current {@code ByteBuffer}
		 * @return The {@code long} value of this {@code ByteBuffer}
		 */
		public long readLong(ByteBuffer bb) {

			long l = bb.getLong();
			return l;

		}

		/**
		 * Returns a {@code String} object with the given length that represent
		 * the {@code char} values of this {@code ByteBuffer}.
		 * 
		 * @param bb
		 *            - The current {@code ByteBuffer}
		 * @param length
		 *            - The {@code String}'s length
		 * @return The {@code char} values of this {@code ByteBuffer} as a
		 *         {@code String}
		 */
		public String readString(ByteBuffer bb, int length) {

			byte[] bytearray = new byte[length];
			bb.get(bytearray);
			String data = new String(bytearray);
			return data;

		}

		/**
		 * Returns a {@code boolean} of this {@code ByteBuffer}.
		 * 
		 * @param bb
		 *            - The current {@code ByteBuffer}
		 * @return The {@code boolean} value of this {@code ByteBuffer}
		 */
		public boolean readBoolean(ByteBuffer bb) {

			byte b = bb.get();
			boolean bool = (b != 0);
			return bool;

		}

		/**
		 * Return a {@code short} value of this {@code ByteBuffer}.
		 * 
		 * @param bb
		 *            - The current {@code ByteBuffer}
		 * @return The {@code short} value of this {@code ByteBuffer}
		 */
		public short readShort(ByteBuffer bb) {

			short s = bb.getShort();
			return s;

		}

		/**
		 * Returns a {@code byte} value of this {@code ByteBuffer}.
		 * 
		 * @param bb
		 *            - The current {@code ByteBuffer}
		 * @return The {@code byte} value of this {@code ByteBuffer}
		 */
		public byte readByte(ByteBuffer bb) {

			byte b = bb.get();
			return b;

		}

	}
}
