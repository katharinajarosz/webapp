USE [Machine_Data_DB]
GO
/****** Object:  Schema [error]    Script Date: 23.09.2020 13:56:27 ******/
CREATE SCHEMA [error]
GO
/****** Object:  Schema [machine]    Script Date: 23.09.2020 13:56:27 ******/
CREATE SCHEMA [machine]
GO
/****** Object:  Schema [term]    Script Date: 23.09.2020 13:56:27 ******/
CREATE SCHEMA [term]
GO
/****** Object:  Schema [ui]    Script Date: 23.09.2020 13:56:27 ******/
CREATE SCHEMA [ui]
GO
/****** Object:  UserDefinedTableType [machine].[AnalogInTemporary]    Script Date: 04.11.2019 15:56:33 ******/
CREATE TYPE [machine].[AnalogInTemporary] AS TABLE(
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[type] [varchar](255) NOT NULL,
	[converted_data] [float] NULL,
	[filtered_data] [float] NULL,
	[machine_run_id] [bigint] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [machine].[BalancingDataTemporary]    Script Date: 04.11.2019 15:56:33 ******/
CREATE TYPE [machine].[BalancingDataTemporary] AS TABLE(
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[acid_flow_conrol] [float] NULL,
	[acid_weight] [float] NULL,
	[base_flow_control] [float] NULL,
	[base_weight] [float] NULL,
	[scale1row] [float] NULL,
	[scale2row] [float] NULL,
	[scale_deviation] [float] NULL,
	[scale_sum_current] [float] NULL,
	[uf_control] [float] NULL,
	[uf_host] [float] NULL,
	[uf_weight] [float] NULL,
	[balance_offset] [float] NULL,	
	[machine_run_id] [bigint] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [machine].[ClosedLoopTemporary]    Script Date: 04.11.2019 15:56:33 ******/
CREATE TYPE [machine].[ClosedLoopTemporary] AS TABLE(
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[dem] [float] NULL,
	[e] [float] NULL,
	[hmi] [float] NULL,
	[inp] [float] NULL,
	[procx] [float] NULL,
	[y] [float] NULL,
	[machine_run_id] [bigint] NOT NULL,
	[closed_loop_type_id] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [machine].[DigitalInputTemporary]    Script Date: 04.11.2019 15:56:33 ******/
CREATE TYPE [machine].[DigitalInputTemporary] AS TABLE(
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[cd] [bit] NULL,
	[r] [bit] NULL,
	[machine_run_id] [bigint] NOT NULL,
	[digital_input_type_id] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [machine].[DigitalOutputTemporary]    Script Date: 04.11.2019 15:56:33 ******/
CREATE TYPE [machine].[DigitalOutputTemporary] AS TABLE(
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[dem] [bit] NULL,
	[hmi] [bit] NULL,
	[machine_run_id] [bigint] NOT NULL,
	[digital_output_type_id] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [machine].[NumericInputTemporary]    Script Date: 04.11.2019 15:56:33 ******/
CREATE TYPE [machine].[NumericInputTemporary] AS TABLE(
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[cd] [float] NULL,
	[f] [float] NULL,
	[r] [float] NULL,
	[machine_run_id] [bigint] NOT NULL,
	[numeric_input_type_id] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [machine].[NumericOutputTemporary]    Script Date: 04.11.2019 15:56:33 ******/
CREATE TYPE [machine].[NumericOutputTemporary] AS TABLE(
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[cd] [float] NULL,
	[dem] [float] NULL,
	[machine_run_id] [bigint] NOT NULL,
	[numeric_output_type_id] [int] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [machine].[PumpDataTemporary]    Script Date: 04.11.2019 15:56:33 ******/
CREATE TYPE [machine].[PumpDataTemporary] AS TABLE(
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[type] [varchar](255) NOT NULL,
	[control] [float] NULL,
	[host] [float] NULL,
	[measure] [float] NULL,
	[machine_run_id] [bigint] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [machine].[SondDataTemporary]    Script Date: 04.11.2019 15:56:33 ******/
CREATE TYPE [machine].[SondDataTemporary] AS TABLE(
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[type] [varchar](255) NOT NULL,
	[calculated_ph] [float] NULL,
	[calculated_temperature] [float] NULL,
	[measured_temperature] [int] NULL,
	[measured_ph] [int] NULL,
	[machine_run_id] [bigint] NOT NULL
)
GO
/****** Object:  UserDefinedTableType [machine].[SystemDiagnosisTemporary]    Script Date: 04.11.2019 15:56:33 ******/
CREATE TYPE [machine].[SystemDiagnosisTemporary] AS TABLE(
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[host_lk_bytes] [float] NULL,
	[host_lk_processor_load] [float] NULL,
	[host_reconnect_counter] [float] NULL,
	[host_processor_load] [float] NULL,
	[loop_late_counter] [float] NULL,
	[memory_load] [float] NULL,
	[processor_load] [float] NULL,
	[system_loop_processing_time] [float] NULL,
	[machine_run_id] [bigint] NOT NULL
)
GO
/****** Object:  Table [dbo].[address]    Script Date: 24.09.2020 09:52:45 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[address](
	[address_id] [bigint] IDENTITY(1,1) NOT NULL,
	[city] [varchar](255) NULL,
	[number] [varchar](255) NULL,
	[plz] [int] NOT NULL,
	[street] [varchar](255) NULL,
CONSTRAINT [PK_address] PRIMARY KEY CLUSTERED 
(
	[address_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[clinics]    Script Date: 24.09.2020 09:52:59 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[clinics](
	[clinic_id] [bigint] IDENTITY(1,1) NOT NULL,
	[clinic_name] [varchar](255) NULL,
	[address_id] [bigint] NULL,
CONSTRAINT [PK_clinics] PRIMARY KEY CLUSTERED 
(
	[clinic_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[closed_loop_type]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[closed_loop_type](
	[closed_loop_type_id] [int] IDENTITY(0,1) NOT NULL,
	[identifier] [varchar](255) NULL,
CONSTRAINT [PK_closed_loop_type] PRIMARY KEY CLUSTERED 
(
	[closed_loop_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[digital_input_type]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[digital_input_type](
	[digital_input_type_id] [int] IDENTITY(0,1) NOT NULL,
	[identifier] [varchar](255) NULL,
CONSTRAINT [PK_digital_input_type] PRIMARY KEY CLUSTERED 
(
	[digital_input_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[digital_output_type]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[digital_output_type](
	[digital_output_type_id] [int] IDENTITY(0,1) NOT NULL,
	[identifier] [varchar](255) NULL,
CONSTRAINT [PK_digital_output_type] PRIMARY KEY CLUSTERED 
(
	[digital_output_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[gp_mode_type]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[gp_mode_type](
	[gp_mode_type_id] [int] IDENTITY(0,1) NOT NULL,
	[identifier] [varchar](255) NULL,
CONSTRAINT [PK_gp_mode_type] PRIMARY KEY CLUSTERED 
(
	[gp_mode_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[heater_mode_type]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[heater_mode_type](
	[heater_mode_type_id] [int] IDENTITY(0,1) NOT NULL,
	[identifier] [varchar](255) NULL,
CONSTRAINT [PK_heater_mode_type] PRIMARY KEY CLUSTERED 
(
	[heater_mode_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[internationalization]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[internationalization](
	[iso_id] [char](3) NOT NULL,
	[Part2B] [char](3) NULL,
	[Part2T] [char](3) NULL,
	[Part1] [char](2) NULL,
	[Scope] [char](1) NOT NULL,
	[Language_Type] [char](1) NOT NULL,
	[Ref_Name] [nvarchar](150) NOT NULL,
	[Comment] [nvarchar](150) NULL,
CONSTRAINT [PK_internationalization] PRIMARY KEY CLUSTERED 
(
	[iso_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[machine_run_calibraton_data]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[machine_run_calibraton_data](
	[machine_run_id] [bigint] NOT NULL,
	[calibration_data_id] [bigint] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[numeric_input_type]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[numeric_input_type](
	[numeric_input_type_id] [int] IDENTITY(0,1) NOT NULL,
	[identifier] [varchar](255) NULL,
CONSTRAINT [PK_numeric_input_type] PRIMARY KEY CLUSTERED 
(
	[numeric_input_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[numeric_output_type]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[numeric_output_type](
	[numeric_output_type_id] [int] IDENTITY(0,1) NOT NULL,
	[identifier] [varchar](255) NULL,
CONSTRAINT [PK_numeric_output_type] PRIMARY KEY CLUSTERED 
(
	[numeric_output_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pp_supply_mode_type]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pp_supply_mode_type](
	[pp_supply_mode_type_id] [int] IDENTITY(0,1) NOT NULL,
	[identifier] [varchar](255) NULL,
CONSTRAINT [PK_pp_supply_mode_type] PRIMARY KEY CLUSTERED 
(
	[pp_supply_mode_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[pp_waste_mode_type]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[pp_waste_mode_type](
	[pp_waste_mode_type_id] [int] IDENTITY(0,1) NOT NULL,
	[identifier] [varchar](255) NULL,
CONSTRAINT [PK_pp_waste_mode_type] PRIMARY KEY CLUSTERED 
(
	[pp_waste_mode_type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [error].[audience]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [error].[audience](
	[audience_id] [int] IDENTITY(1,1) NOT NULL,
	[audience] [nvarchar](255) NULL,
CONSTRAINT [PK_audience] PRIMARY KEY CLUSTERED 
(
	[audience_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [error].[catchphrase]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [error].[catchphrase](
	[catchphrase_id] [int] IDENTITY(1,1) NOT NULL,
	[translation] [nvarchar](600) NULL,
	[description] [nvarchar](600) NULL,
	[iso_id] [char](3) NULL,
CONSTRAINT [PK_catchphrase] PRIMARY KEY CLUSTERED 
(
	[catchphrase_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [error].[description]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [error].[description](
	[description_id] [int] IDENTITY(1,1) NOT NULL,
	[translation] [nvarchar](600) NULL,
	[iso_id] [char](3) NULL,
CONSTRAINT [PK_description] PRIMARY KEY CLUSTERED 
(
	[description_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [error].[error]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [error].[error](
	[error_id] [int] NOT NULL,
	[error_code] [nvarchar](16) NULL,
	[state] [nvarchar](255) NULL,
	[condition] [nvarchar](600) NULL,
	[escalation] [int] NULL,
	[deprecated] [bit] NOT NULL,
	[comment] [nvarchar](600) NULL,
	[abortion] [bit] NULL,
	[reviewer] [nvarchar](255) NULL,
	[reviewed] [datetime] NULL,
	[acknowledge] [bit] NULL,								
	[severity_id] [int] NULL,
CONSTRAINT [PK_error] PRIMARY KEY CLUSTERED 
(
	[error_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [error].[error_catchphrase]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [error].[error_catchphrase](
	[fk_catchphrase] [int] NOT NULL,
	[fk_error] [int] NOT NULL,
	[comment] [nvarchar](600) NULL,
	[weight] [float] NULL,
CONSTRAINT [PK_error_catchphrase] PRIMARY KEY CLUSTERED 
(
	[fk_catchphrase] ASC,
	[fk_error] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [error].[error_description]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [error].[error_description](
	[fk_audience] [int] NOT NULL,
	[fk_description] [int] NOT NULL,
	[fk_error] [int] NOT NULL,
CONSTRAINT [PK_error_description] PRIMARY KEY CLUSTERED 
(
	[fk_audience] ASC,
	[fk_description] ASC,
	[fk_error] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [error].[error_solution]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [error].[error_solution](
	[fk_audience] [int] NOT NULL,
	[fk_error] [int] NOT NULL,
	[fk_solution] [int] NOT NULL,
	[asc_order] [int] NULL,
CONSTRAINT [PK_error_solution] PRIMARY KEY CLUSTERED 
(
	[fk_audience] ASC,
	[fk_error] ASC,
	[fk_solution] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [error].[severity]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [error].[severity](
	[severity_id] [int] IDENTITY(0,1) NOT NULL,
	[identifier] [nvarchar](255) NULL,
	[severity] [nvarchar](255) NULL,
CONSTRAINT [PK_severity] PRIMARY KEY CLUSTERED 
(
	[severity_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [error].[solution]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [error].[solution](
	[solution_id] [int] IDENTITY(1,1) NOT NULL,
	[translation] [nvarchar](600) NULL,
	[iso_id] [char](3) NULL,
CONSTRAINT [PK_solution] PRIMARY KEY CLUSTERED 
(
	[solution_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [machine].[active_error]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [machine].[active_error](
	[active_error_id] [bigint] IDENTITY(1,1) NOT NULL,
	[start] [datetime2](7) NULL,
	[ende] [datetime2](7) NULL,
	[error_id] [int] NULL,
	[machine_run_id] [bigint] NULL,
	[severity_id] [int] NULL,
CONSTRAINT [PK_active_error] PRIMARY KEY CLUSTERED 
(
	[active_error_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [machine].[analog_in]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [machine].[analog_in](
	[analog_in_id] [bigint] IDENTITY(1,1) NOT NULL,
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[type] [varchar](255) NOT NULL,
	[converted_data] [float] NULL,
	[filtered_data] [float] NULL,
	[machine_run_id] [bigint] NOT NULL,
CONSTRAINT [PK_analog_in] PRIMARY KEY CLUSTERED 
(
	[analog_in_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [machine].[balancing_data]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [machine].[balancing_data](
	[balancing_data_id] [bigint] IDENTITY(1,1) NOT NULL,
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[acid_flow_conrol] [float] NULL,
	[acid_weight] [float] NULL,
	[base_flow_control] [float] NULL,
	[base_weight] [float] NULL,
	[scale1row] [float] NULL,
	[scale2row] [float] NULL,
	[scale_deviation] [float] NULL,
	[scale_sum_current] [float] NULL,
	[uf_control] [float] NULL,
	[uf_host] [float] NULL,
	[uf_weight] [float] NULL,
	[balance_offset] [float] NULL,	
	[machine_run_id] [bigint] NOT NULL,
CONSTRAINT [PK_balancing_data] PRIMARY KEY CLUSTERED 
(
	[balancing_data_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [machine].[calibration_data]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [machine].[calibration_data](
	[calibration_data_id] [bigint] IDENTITY(1,1) NOT NULL,
	[type] [varchar](255) NULL,
	[value] [float] NULL,
CONSTRAINT [PK_calibration_data] PRIMARY KEY CLUSTERED 
(
	[calibration_data_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [machine].[closed_loop]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [machine].[closed_loop](
	[closed_loop_id] [bigint] IDENTITY(1,1) NOT NULL,
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[dem] [float] NULL,
	[e] [float] NULL,
	[hmi] [float] NULL,
	[inp] [float] NULL,
	[procx] [float] NULL,
	[y] [float] NULL,
	[machine_run_id] [bigint] NOT NULL,
	[closed_loop_type_id] [int] NOT NULL,
CONSTRAINT [PK_closed_loop] PRIMARY KEY CLUSTERED 
(
	[closed_loop_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [machine].[digital_input]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [machine].[digital_input](
	[digital_input_id] [bigint] IDENTITY(1,1) NOT NULL,
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[cd] [bit] NULL,
	[r] [bit] NULL,
	[machine_run_id] [bigint] NOT NULL,
	[digital_input_type_id] [int] NOT NULL,
CONSTRAINT [PK_digital_input] PRIMARY KEY CLUSTERED 
(
	[digital_input_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [machine].[digital_output]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [machine].[digital_output](
	[digital_output_id] [bigint] IDENTITY(1,1) NOT NULL,
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[dem] [bit] NULL,
	[hmi] [bit] NULL,
	[machine_run_id] [bigint] NOT NULL,
	[digital_output_type_id] [int] NOT NULL,
CONSTRAINT [PK_digital_output] PRIMARY KEY CLUSTERED 
(
	[digital_output_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [machine].[iodata]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [machine].[iodata](
	[io_data_id] [bigint] IDENTITY(1,1) NOT NULL,
	[start] [datetime2](7) NULL,
	[ende] [datetime2](7) NULL,	
	[gp_mode_type_id] [int] NULL,
	[heater_mode_type_id] [int] NULL,
	[machine_run_id] [bigint] NOT NULL,
	[pp_supply_mode_type_id] [int] NULL,
	[pp_waste_mode_type_id] [int] NULL,
CONSTRAINT [PK_iodata] PRIMARY KEY CLUSTERED 
(
	[io_data_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [machine].[machine_run]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [machine].[machine_run](
	[machine_run_id] [bigint] IDENTITY(1,1) NOT NULL,
	[start] [datetime2](7) NULL,
	[ende] [datetime2](7) NULL,
	[file_name] [varchar](255) NULL,	
	[sond_data_config_id] [bigint] NULL,
CONSTRAINT [PK_machine_run] PRIMARY KEY CLUSTERED 
(
	[machine_run_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [machine].[numeric_input]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [machine].[numeric_input](
	[numeric_input_id] [bigint] IDENTITY(1,1) NOT NULL,	
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[cd] [float] NULL,
	[f] [float] NULL,
	[r] [float] NULL,
	[machine_run_id] [bigint] NOT NULL,
	[numeric_input_type_id] [int] NOT NULL,
CONSTRAINT [PK_numeric_input] PRIMARY KEY CLUSTERED 
(
	[numeric_input_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [machine].[numeric_output]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [machine].[numeric_output](
	[numeric_output_id] [bigint] IDENTITY(1,1) NOT NULL,	
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[cd] [float] NULL,
	[dem] [float] NULL,
	[machine_run_id] [bigint] NOT NULL,
	[numeric_output_type_id] [int] NOT NULL,
CONSTRAINT [PK_numeric_output] PRIMARY KEY CLUSTERED 
(
	[numeric_output_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [machine].[protect_state]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [machine].[protect_state](
	[protect_state_id] [bigint] IDENTITY(1,1) NOT NULL,	
	[protect_state] [int] NULL,
	[protect_substate] [int] NULL,
	[start] [datetime2](7) NULL,
	[ende] [datetime2](7) NULL,
	[machine_run_id] [bigint] NOT NULL,
CONSTRAINT [PK_protect_state] PRIMARY KEY CLUSTERED 
(
	[protect_state_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [machine].[psconfiguration]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [machine].[psconfiguration](
	[machine_run_id] [bigint] NOT NULL,
	[acid_density] [float] NULL,
	[base_density] [float] NULL,
CONSTRAINT [PK_psconfiguration] PRIMARY KEY CLUSTERED 
(
	[machine_run_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [machine].[psmissed_message]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [machine].[psmissed_message](
	[ps_missed_message_id] [bigint] IDENTITY(1,1) NOT NULL,	
	[message_counter_error_count] [int] NULL,
	[send_message_counter] [int] NULL,
	[start] [datetime2](7) NULL,
	[ende] [datetime2](7) NULL,
	[machine_run_id] [bigint] NOT NULL,
CONSTRAINT [PK_psmissed_message] PRIMARY KEY CLUSTERED 
(
	[ps_missed_message_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [machine].[psstatus]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [machine].[psstatus](
	[ps_status_id] [bigint] IDENTITY(1,1) NOT NULL,
	[control_status_byte] [smallint] NULL,	
	[protect_status_byte] [smallint] NULL,
	[start] [datetime2](7) NULL,
	[ende] [datetime2](7) NULL,
	[machine_run_id] [bigint] NOT NULL,
CONSTRAINT [PK_psstatus] PRIMARY KEY CLUSTERED 
(
	[ps_status_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [machine].[pump_data]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [machine].[pump_data](
	[pump_data_id] [bigint] IDENTITY(1,1) NOT NULL,
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[type] [varchar](255) NOT NULL,
	[control] [float] NULL,
	[host] [float] NULL,
	[measure] [float] NULL,
	[machine_run_id] [bigint] NOT NULL,
CONSTRAINT [PK_pump_data] PRIMARY KEY CLUSTERED 
(
	[pump_data_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [machine].[running_system_state]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [machine].[running_system_state](
	[running_system_state_id] [bigint] IDENTITY(1,1) NOT NULL,
	[start] [datetime2](7) NULL,
	[ende] [datetime2](7) NULL,
	[system_state_node] [hierarchyid] NULL,
	[subsequence_node] [hierarchyid] NULL,	
	[machine_run_id] [bigint] NOT NULL,		
CONSTRAINT [PK_running_system_state] PRIMARY KEY CLUSTERED 
(
	[running_system_state_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [machine].[sond_data]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [machine].[sond_data](
	[sond_data_id] [bigint] IDENTITY(1,1) NOT NULL,
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[type] [varchar](255) NOT NULL,
	[calculated_ph] [float] NULL,
	[calculated_temperature] [float] NULL,
	[measured_temperature] [int] NULL,
	[measured_ph] [int] NULL,
	[machine_run_id] [bigint] NOT NULL,
CONSTRAINT [PK_sond_data] PRIMARY KEY CLUSTERED 
(
	[sond_data_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [machine].[sond_data_configuration]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [machine].[sond_data_configuration](
	[sond_data_configuration_id] [bigint] IDENTITY(1,1) NOT NULL,
	[serial_number] [int] NULL,
CONSTRAINT [PK_sond_data_configuration] PRIMARY KEY CLUSTERED 
(
	[sond_data_configuration_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [machine].[system_diagnosis]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [machine].[system_diagnosis](
	[system_diagnosis_id] [bigint] IDENTITY(1,1) NOT NULL,
	[machine_data_loop_timestamp] [datetime2](7) NOT NULL,
	[host_lk_bytes] [float] NULL,
	[host_lk_processor_load] [float] NULL,
	[host_reconnect_counter] [float] NULL,
	[host_processor_load] [float] NULL,
	[loop_late_counter] [float] NULL,
	[memory_load] [float] NULL,
	[processor_load] [float] NULL,
	[system_loop_processing_time] [float] NULL,
	[machine_run_id] [bigint] NOT NULL,
CONSTRAINT [PK_system_diagnosis] PRIMARY KEY CLUSTERED 
(
	[system_diagnosis_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [machine].[system_state]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [machine].[system_state](
	[system_state_node] [hierarchyid] NOT NULL,
	[node_string]  AS ([System_State_Node].[ToString]()),
	[system_state_level]  AS ([System_State_Node].[GetLevel]()),
	[system_state_name] [varchar](255) NULL,
	[system_state_value] [int] NULL,
CONSTRAINT [PK_system_state] PRIMARY KEY CLUSTERED 
(
	[system_state_node] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [machine].[system_state_subsequence]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [machine].[system_state_subsequence](
	[subsequence_node] [hierarchyid] NOT NULL,
	[subsequence_node_string]  AS ([Subsequence_Node].[ToString]()),
	[subsequence_id] [int] NULL,
	[subsequence_level]  AS ([Subsequence_Node].[GetLevel]()),
	[subsequence_name] [varchar](255) NULL,
CONSTRAINT [PK_system_state_subsequence] PRIMARY KEY CLUSTERED 
(
	[subsequence_node] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [term].[status]    Script Date: 27.09.2020 18:14:12 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [term].[status](
	[status_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NULL,
CONSTRAINT [PK_term_status] PRIMARY KEY CLUSTERED 
(
	[status_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [term].[term]    Script Date: 27.09.2020 18:14:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [term].[term](
	[term_id] [hierarchyid] NOT NULL,
	[term_constraint_id] [int] NULL,
	[status_id] [int] NULL,
	[type_id] [int] NULL,
	[usage_id] [int] NULL,
CONSTRAINT [PK_term] PRIMARY KEY CLUSTERED 
(
	[term_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [term].[term_constraint]    Script Date: 27.09.2020 18:14:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [term].[term_constraint](
	[term_constraint_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NULL,
CONSTRAINT [PK_term_constraint] PRIMARY KEY CLUSTERED 
(
	[term_constraint_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [term].[type]    Script Date: 27.09.2020 18:14:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [term].[type](
	[type_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NULL,
CONSTRAINT [PK_term_type] PRIMARY KEY CLUSTERED 
(
	[type_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [term].[usage]    Script Date: 27.09.2020 18:14:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [term].[usage](
	[usage_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NULL,
CONSTRAINT [PK_term_usage] PRIMARY KEY CLUSTERED 
(
	[usage_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [ui].[component]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ui].[component](
	[component_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NULL,
CONSTRAINT [PK_component] PRIMARY KEY CLUSTERED 
(
	[component_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [ui].[event]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ui].[event](
	[event_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NULL,
CONSTRAINT [PK_event] PRIMARY KEY CLUSTERED 
(
	[event_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [ui].[identifier]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ui].[identifier](
	[identifier_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NULL,
CONSTRAINT [PK_identifier] PRIMARY KEY CLUSTERED 
(
	[identifier_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [ui].[uilog]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ui].[uilog](
	[uilog_id] [bigint] IDENTITY(1,1) NOT NULL,
	[timestamp] [datetime2](7) NULL,
	[component_id] [int] NULL,
	[event_id] [int] NULL,
	[identifier_id] [int] NULL,
	[machine_run_id] [bigint] NULL,
	[value_id] [int] NULL,
CONSTRAINT [PK_uilog] PRIMARY KEY CLUSTERED 
(
	[uilog_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [ui].[value]    Script Date: 23.09.2020 13:56:27 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [ui].[value](
	[value_id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NULL,
CONSTRAINT [PK_value] PRIMARY KEY CLUSTERED 
(
	[value_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/**/
ALTER TABLE [dbo].[clinics]  WITH CHECK ADD  CONSTRAINT [FK_clinics_address] FOREIGN KEY([address_id])
REFERENCES [dbo].[address] ([address_id])
GO
ALTER TABLE [dbo].[clinics] CHECK CONSTRAINT [FK_clinics_address]
GO
/**/
ALTER TABLE [dbo].[machine_run_calibraton_data]  WITH CHECK ADD  CONSTRAINT [FK_machine_run_calibraton_data_machine_run] FOREIGN KEY([machine_run_id])
REFERENCES [machine].[machine_run] ([machine_run_id])
GO
ALTER TABLE [dbo].[machine_run_calibraton_data] CHECK CONSTRAINT [FK_machine_run_calibraton_data_machine_run]
GO
ALTER TABLE [dbo].[machine_run_calibraton_data]  WITH CHECK ADD  CONSTRAINT [FK_machine_run_calibraton_data_calibration_data] FOREIGN KEY([calibration_data_id])
REFERENCES [machine].[calibration_data] ([calibration_data_id])
GO
ALTER TABLE [dbo].[machine_run_calibraton_data] CHECK CONSTRAINT [FK_machine_run_calibraton_data_calibration_data]
GO
/**/
ALTER TABLE [error].[catchphrase]  WITH CHECK ADD  CONSTRAINT [FK_catchphrase_internationalization] FOREIGN KEY([iso_id])
REFERENCES [dbo].[internationalization] ([iso_id])
GO
ALTER TABLE [error].[catchphrase] CHECK CONSTRAINT [FK_catchphrase_internationalization]
GO
/**/
ALTER TABLE [error].[description]  WITH CHECK ADD  CONSTRAINT [FK_description_internationalization] FOREIGN KEY([iso_id])
REFERENCES [dbo].[internationalization] ([iso_id])
GO
ALTER TABLE [error].[description] CHECK CONSTRAINT [FK_description_internationalization]
GO
/**/
ALTER TABLE [error].[error] ADD  CONSTRAINT [DF_error_deprecated]  DEFAULT ((0)) FOR [deprecated]
GO
ALTER TABLE [error].[error]  WITH CHECK ADD  CONSTRAINT [FK_error_severity] FOREIGN KEY([severity_id])
REFERENCES [error].[severity] ([severity_id])
GO
ALTER TABLE [error].[error] CHECK CONSTRAINT [FK_error_severity]
GO
/**/
ALTER TABLE [error].[error_catchphrase]  WITH CHECK ADD  CONSTRAINT [FK_error_catchphrase_error] FOREIGN KEY([fk_error])
REFERENCES [error].[error] ([error_id])
GO
ALTER TABLE [error].[error_catchphrase] CHECK CONSTRAINT [FK_error_catchphrase_error]
GO
ALTER TABLE [error].[error_catchphrase]  WITH CHECK ADD  CONSTRAINT [FK_error_catchphrase_catchphrase] FOREIGN KEY([fk_catchphrase])
REFERENCES [error].[catchphrase] ([catchphrase_id])
GO
ALTER TABLE [error].[error_catchphrase] CHECK CONSTRAINT [FK_error_catchphrase_catchphrase]
GO
/**/
ALTER TABLE [error].[error_description]  WITH CHECK ADD  CONSTRAINT [FK_error_description_error] FOREIGN KEY([fk_error])
REFERENCES [error].[error] ([error_id])
GO
ALTER TABLE [error].[error_description] CHECK CONSTRAINT [FK_error_description_error]
GO
ALTER TABLE [error].[error_description]  WITH CHECK ADD  CONSTRAINT [FK_error_description_description] FOREIGN KEY([fk_description])
REFERENCES [error].[description] ([description_id])
GO
ALTER TABLE [error].[error_description] CHECK CONSTRAINT [FK_error_description_description]
GO
ALTER TABLE [error].[error_description]  WITH CHECK ADD  CONSTRAINT [FK_error_description_audience] FOREIGN KEY([fk_audience])
REFERENCES [error].[audience] ([audience_id])
GO
ALTER TABLE [error].[error_description] CHECK CONSTRAINT [FK_error_description_audience]
GO
/**/
ALTER TABLE [error].[error_solution]  WITH CHECK ADD  CONSTRAINT [FK_error_solution_error] FOREIGN KEY([fk_error])
REFERENCES [error].[error] ([error_id])
GO
ALTER TABLE [error].[error_solution] CHECK CONSTRAINT [FK_error_solution_error]
GO
ALTER TABLE [error].[error_solution]  WITH CHECK ADD  CONSTRAINT [FK_error_solution_audience] FOREIGN KEY([fk_audience])
REFERENCES [error].[audience] ([audience_id])
GO
ALTER TABLE [error].[error_solution] CHECK CONSTRAINT [FK_error_solution_audience]
GO
ALTER TABLE [error].[error_solution]  WITH CHECK ADD  CONSTRAINT [FK_error_solution_solution] FOREIGN KEY([fk_solution])
REFERENCES [error].[solution] ([solution_id])
GO
ALTER TABLE [error].[error_solution] CHECK CONSTRAINT [FK_error_solution_solution]
GO
/**/
ALTER TABLE [error].[solution]  WITH CHECK ADD  CONSTRAINT [FK_solution_internationalization] FOREIGN KEY([iso_id])
REFERENCES [dbo].[internationalization] ([iso_id])
GO
ALTER TABLE [error].[solution] CHECK CONSTRAINT [FK_solution_internationalization]
GO
/**/
ALTER TABLE [machine].[active_error]  WITH CHECK ADD  CONSTRAINT [FK_active_error_severity] FOREIGN KEY([severity_id])
REFERENCES [error].[severity] ([severity_id])
GO
ALTER TABLE [machine].[active_error] CHECK CONSTRAINT [FK_active_error_severity]
GO
ALTER TABLE [machine].[active_error]  WITH CHECK ADD  CONSTRAINT [FK_active_error_machine_run] FOREIGN KEY([machine_run_id])
REFERENCES [machine].[machine_run] ([machine_run_id])
GO
ALTER TABLE [machine].[active_error] CHECK CONSTRAINT [FK_active_error_machine_run]
GO
ALTER TABLE [machine].[active_error]  WITH CHECK ADD  CONSTRAINT [FK_active_error_error] FOREIGN KEY([error_id])
REFERENCES [error].[error] ([error_id])
GO
ALTER TABLE [machine].[active_error] CHECK CONSTRAINT [FK_active_error_error]
GO
/**/
ALTER TABLE [machine].[analog_in]  WITH CHECK ADD  CONSTRAINT [FK_analog_in_machine_run] FOREIGN KEY([machine_run_id])
REFERENCES [machine].[machine_run] ([machine_run_id])
GO
ALTER TABLE [machine].[analog_in] CHECK CONSTRAINT [FK_analog_in_machine_run]
GO
/**/
ALTER TABLE [machine].[balancing_data]  WITH CHECK ADD  CONSTRAINT [FK_balancing_data_machine_run] FOREIGN KEY([machine_run_id])
REFERENCES [machine].[machine_run] ([machine_run_id])
GO
ALTER TABLE [machine].[balancing_data] CHECK CONSTRAINT [FK_balancing_data_machine_run]
GO
/**/
ALTER TABLE [machine].[closed_loop]  WITH CHECK ADD  CONSTRAINT [FK_closed_loop_closed_loop_type] FOREIGN KEY([closed_loop_type_id])
REFERENCES [dbo].[closed_loop_type] ([closed_loop_type_id])
GO
ALTER TABLE [machine].[closed_loop] CHECK CONSTRAINT [FK_closed_loop_closed_loop_type]
GO
ALTER TABLE [machine].[closed_loop]  WITH CHECK ADD  CONSTRAINT [FK_closed_loop_machine_run] FOREIGN KEY([machine_run_id])
REFERENCES [machine].[machine_run] ([machine_run_id])
GO
ALTER TABLE [machine].[closed_loop] CHECK CONSTRAINT [FK_closed_loop_machine_run]
GO
/**/
ALTER TABLE [machine].[digital_input]  WITH CHECK ADD  CONSTRAINT [FK_digital_input_digital_input_type] FOREIGN KEY([digital_input_type_id])
REFERENCES [dbo].[digital_input_type] ([digital_input_type_id])
GO
ALTER TABLE [machine].[digital_input] CHECK CONSTRAINT [FK_digital_input_digital_input_type]
GO
ALTER TABLE [machine].[digital_input]  WITH CHECK ADD  CONSTRAINT [FK_digital_input_machine_run] FOREIGN KEY([machine_run_id])
REFERENCES [machine].[machine_run] ([machine_run_id])
GO
ALTER TABLE [machine].[digital_input] CHECK CONSTRAINT [FK_digital_input_machine_run]
GO
/**/
ALTER TABLE [machine].[digital_output]  WITH CHECK ADD  CONSTRAINT [FK_digital_output_digital_output_type] FOREIGN KEY([digital_output_type_id])
REFERENCES [dbo].[digital_output_type] ([digital_output_type_id])
GO
ALTER TABLE [machine].[digital_output] CHECK CONSTRAINT [FK_digital_output_digital_output_type]
GO
ALTER TABLE [machine].[digital_output]  WITH CHECK ADD  CONSTRAINT [FK_digital_output_machine_run] FOREIGN KEY([machine_run_id])
REFERENCES [machine].[machine_run] ([machine_run_id])
GO
ALTER TABLE [machine].[digital_output] CHECK CONSTRAINT [FK_digital_output_machine_run]
GO
/**/
ALTER TABLE [machine].[iodata]  WITH CHECK ADD  CONSTRAINT [FK_iodata_pp_supply_mode_type] FOREIGN KEY([pp_supply_mode_type_id])
REFERENCES [dbo].[pp_supply_mode_type] ([pp_supply_mode_type_id])
GO
ALTER TABLE [machine].[iodata] CHECK CONSTRAINT [FK_iodata_pp_supply_mode_type]
GO
ALTER TABLE [machine].[iodata]  WITH CHECK ADD  CONSTRAINT [FK_iodata_heater_mode_type] FOREIGN KEY([heater_mode_type_id])
REFERENCES [dbo].[heater_mode_type] ([heater_mode_type_id])
GO
ALTER TABLE [machine].[iodata] CHECK CONSTRAINT [FK_iodata_heater_mode_type]
GO
ALTER TABLE [machine].[iodata]  WITH CHECK ADD  CONSTRAINT [FK_iodata_machine_run] FOREIGN KEY([machine_run_id])
REFERENCES [machine].[machine_run] ([machine_run_id])
GO
ALTER TABLE [machine].[iodata] CHECK CONSTRAINT [FK_iodata_machine_run]
GO
ALTER TABLE [machine].[iodata]  WITH CHECK ADD  CONSTRAINT [FK_iodata_pp_waste_mode_type] FOREIGN KEY([pp_waste_mode_type_id])
REFERENCES [dbo].[pp_waste_mode_type] ([pp_waste_mode_type_id])
GO
ALTER TABLE [machine].[iodata] CHECK CONSTRAINT [FK_iodata_pp_waste_mode_type]
GO
ALTER TABLE [machine].[iodata]  WITH CHECK ADD  CONSTRAINT [FK_iodata_gp_mode_type] FOREIGN KEY([gp_mode_type_id])
REFERENCES [dbo].[gp_mode_type] ([gp_mode_type_id])
GO
ALTER TABLE [machine].[iodata] CHECK CONSTRAINT [FK_iodata_gp_mode_type]
GO
/**/
ALTER TABLE [machine].[machine_run]  WITH CHECK ADD  CONSTRAINT [FK_machine_run_sond_data_configuration] FOREIGN KEY([sond_data_config_id])
REFERENCES [machine].[sond_data_configuration] ([sond_data_configuration_id])
GO
ALTER TABLE [machine].[machine_run] CHECK CONSTRAINT [FK_machine_run_sond_data_configuration]
GO
/**/
ALTER TABLE [machine].[numeric_input]  WITH CHECK ADD  CONSTRAINT [FK_numeric_input_numeric_input_type] FOREIGN KEY([numeric_input_type_id])
REFERENCES [dbo].[numeric_input_type] ([numeric_input_type_id])
GO
ALTER TABLE [machine].[numeric_input] CHECK CONSTRAINT [FK_numeric_input_numeric_input_type]
GO
ALTER TABLE [machine].[numeric_input]  WITH CHECK ADD  CONSTRAINT [FK_numeric_input_machine_run] FOREIGN KEY([machine_run_id])
REFERENCES [machine].[machine_run] ([machine_run_id])
GO
ALTER TABLE [machine].[numeric_input] CHECK CONSTRAINT [FK_numeric_input_machine_run]
GO
/**/
ALTER TABLE [machine].[numeric_output]  WITH CHECK ADD  CONSTRAINT [FK_numeric_output_machine_run] FOREIGN KEY([machine_run_id])
REFERENCES [machine].[machine_run] ([machine_run_id])
GO
ALTER TABLE [machine].[numeric_output] CHECK CONSTRAINT [FK_numeric_output_machine_run]
GO
ALTER TABLE [machine].[numeric_output]  WITH CHECK ADD  CONSTRAINT [FK_numeric_output_numeric_output_type] FOREIGN KEY([numeric_output_type_id])
REFERENCES [dbo].[numeric_output_type] ([numeric_output_type_id])
GO
ALTER TABLE [machine].[numeric_output] CHECK CONSTRAINT [FK_numeric_output_numeric_output_type]
GO
/**/
ALTER TABLE [machine].[protect_state]  WITH CHECK ADD  CONSTRAINT [FK_protect_state_machine_run] FOREIGN KEY([machine_run_id])
REFERENCES [machine].[machine_run] ([machine_run_id])
GO
ALTER TABLE [machine].[protect_state] CHECK CONSTRAINT [FK_protect_state_machine_run]
GO
/**/
ALTER TABLE [machine].[psconfiguration]  WITH CHECK ADD  CONSTRAINT [FK_psconfiguration_machine_run] FOREIGN KEY([machine_run_id])
REFERENCES [machine].[machine_run] ([machine_run_id])
GO
ALTER TABLE [machine].[psconfiguration] CHECK CONSTRAINT [FK_psconfiguration_machine_run]
GO
/**/
ALTER TABLE [machine].[psmissed_message]  WITH CHECK ADD  CONSTRAINT [FK_psmissed_message_machine_run] FOREIGN KEY([machine_run_id])
REFERENCES [machine].[machine_run] ([machine_run_id])
GO
ALTER TABLE [machine].[psmissed_message] CHECK CONSTRAINT [FK_psmissed_message_machine_run]
GO
/**/
ALTER TABLE [machine].[psstatus]  WITH CHECK ADD  CONSTRAINT [FK_psstatus_machine_run] FOREIGN KEY([machine_run_id])
REFERENCES [machine].[machine_run] ([machine_run_id])
GO
ALTER TABLE [machine].[psstatus] CHECK CONSTRAINT [FK_psstatus_machine_run]
GO
/**/
ALTER TABLE [machine].[pump_data]  WITH CHECK ADD  CONSTRAINT [FK_pump_data_machine_run] FOREIGN KEY([machine_run_id])
REFERENCES [machine].[machine_run] ([machine_run_id])
GO
ALTER TABLE [machine].[pump_data] CHECK CONSTRAINT [FK_pump_data_machine_run]
GO
/**/
ALTER TABLE [machine].[running_system_state]  WITH CHECK ADD  CONSTRAINT [FK_running_system_state_machine_run] FOREIGN KEY([machine_run_id])
REFERENCES [machine].[machine_run] ([machine_run_id])
GO
ALTER TABLE [machine].[running_system_state] CHECK CONSTRAINT [FK_running_system_state_machine_run]
GO
ALTER TABLE [machine].[running_system_state]  WITH CHECK ADD  CONSTRAINT [FK_running_system_state_system_state] FOREIGN KEY([system_state_node])
REFERENCES [machine].[system_state] ([system_state_node])
GO
ALTER TABLE [machine].[running_system_state] CHECK CONSTRAINT [FK_running_system_state_system_state]
GO
ALTER TABLE [machine].[running_system_state]  WITH CHECK ADD  CONSTRAINT [FK_running_system_state_system_state_subsequence] FOREIGN KEY([subsequence_node])
REFERENCES [machine].[system_state_subsequence] ([subsequence_node])
GO
ALTER TABLE [machine].[running_system_state] CHECK CONSTRAINT [FK_running_system_state_system_state_subsequence]
GO
/**/
ALTER TABLE [machine].[sond_data]  WITH CHECK ADD  CONSTRAINT [FK_sond_data_machine_run] FOREIGN KEY([machine_run_id])
REFERENCES [machine].[machine_run] ([machine_run_id])
GO
ALTER TABLE [machine].[sond_data] CHECK CONSTRAINT [FK_sond_data_machine_run]
GO
/**/
ALTER TABLE [machine].[system_diagnosis]  WITH CHECK ADD  CONSTRAINT [FK_system_diagnosis_machine_run] FOREIGN KEY([machine_run_id])
REFERENCES [machine].[machine_run] ([machine_run_id])
GO
ALTER TABLE [machine].[system_diagnosis] CHECK CONSTRAINT [FK_system_diagnosis_machine_run]
GO
/**/
ALTER TABLE [ui].[uilog]  WITH CHECK ADD  CONSTRAINT [FK_uilog_identifier] FOREIGN KEY([identifier_id])
REFERENCES [ui].[identifier] ([identifier_id])
GO
ALTER TABLE [ui].[uilog] CHECK CONSTRAINT [FK_uilog_identifier]
GO
ALTER TABLE [ui].[uilog]  WITH CHECK ADD  CONSTRAINT [FK_uilog_machine_run] FOREIGN KEY([machine_run_id])
REFERENCES [machine].[machine_run] ([machine_run_id])
GO
ALTER TABLE [ui].[uilog] CHECK CONSTRAINT [FK_uilog_machine_run]
GO
ALTER TABLE [ui].[uilog]  WITH CHECK ADD  CONSTRAINT [FK_uilog_value] FOREIGN KEY([value_id])
REFERENCES [ui].[value] ([value_id])
GO
ALTER TABLE [ui].[uilog] CHECK CONSTRAINT [FK_uilog_value]
GO
ALTER TABLE [ui].[uilog]  WITH CHECK ADD  CONSTRAINT [FK_uilog_component] FOREIGN KEY([component_id])
REFERENCES [ui].[component] ([component_id])
GO
ALTER TABLE [ui].[uilog] CHECK CONSTRAINT [FK_uilog_component]
GO
ALTER TABLE [ui].[uilog]  WITH CHECK ADD  CONSTRAINT [FK_uilog_event] FOREIGN KEY([event_id])
REFERENCES [ui].[event] ([event_id])
GO
ALTER TABLE [ui].[uilog] CHECK CONSTRAINT [FK_uilog_event]
GO
/**/
ALTER TABLE [term].[term]  WITH CHECK ADD  CONSTRAINT [FK_term_term_constraint] FOREIGN KEY([term_constraint_id])
REFERENCES [term].[term_constraint] ([term_constraint_id])
GO
ALTER TABLE [term].[term] CHECK CONSTRAINT [FK_term_term_constraint]
GO
ALTER TABLE [term].[term]  WITH CHECK ADD  CONSTRAINT [FK_term_status] FOREIGN KEY([status_id])
REFERENCES [term].[status] ([status_id])
GO
ALTER TABLE [term].[term] CHECK CONSTRAINT [FK_term_status]
GO
ALTER TABLE [term].[term]  WITH CHECK ADD  CONSTRAINT [FK_term_type] FOREIGN KEY([type_id])
REFERENCES [term].[type] ([type_id])
GO
ALTER TABLE [term].[term] CHECK CONSTRAINT [FK_term_type]
GO
ALTER TABLE [term].[term]  WITH CHECK ADD  CONSTRAINT [FK_term_usage] FOREIGN KEY([usage_id])
REFERENCES [term].[usage] ([usage_id])
GO
ALTER TABLE [term].[term] CHECK CONSTRAINT [FK_term_usage]
GO
