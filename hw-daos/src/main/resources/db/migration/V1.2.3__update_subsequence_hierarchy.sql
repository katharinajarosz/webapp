INSERT machine.system_state_subsequence (subsequence_node, subsequence_id, subsequence_name)  
VALUES (hierarchyid::GetRoot(), 0, 'System Subsequence') ;  
GO

IF OBJECT_ID('AddSubsequence','P') IS NOT NULL
   DROP PROCEDURE AddSubsequence
GO

CREATE PROC AddSubsequence(@stateId int OUTPUT, @stateName varchar(50))   
AS   
BEGIN  
   DECLARE @parentNode hierarchyid, @lastChildNode hierarchyid
   SELECT @parentNode = hierarchyid::GetRoot()
   FROM machine.system_state_subsequence
   
   SET TRANSACTION ISOLATION LEVEL SERIALIZABLE  
   BEGIN TRANSACTION   
		
      SELECT @lastChildNode = max(subsequence_node)
		FROM machine.system_state_subsequence
		WHERE subsequence_node.GetAncestor(1) = @parentNode
	  INSERT machine.system_state_subsequence (subsequence_node, subsequence_id, subsequence_name)  
      VALUES(@parentNode.GetDescendant(@lastChildNode, NULL), @stateId, @stateName)
	  SET @stateId = @stateId+1;  
   COMMIT  
END ;  
GO

DECLARE @stateId int = 1;

-- EXEC AddSubsequence @stateId OUTPUT,'<nothing>';
EXEC AddSubsequence @stateId OUTPUT,'Filling';
EXEC AddSubsequence @stateId OUTPUT,'Empty';
EXEC AddSubsequence @stateId OUTPUT,'IntakeCitricAcid';
EXEC AddSubsequence @stateId OUTPUT,'CitricHotRinse';
EXEC AddSubsequence @stateId OUTPUT,'ColdRinse';
EXEC AddSubsequence @stateId OUTPUT,'IntakeSodaCleaner1st';
EXEC AddSubsequence @stateId OUTPUT,'SodaHotRinse';
EXEC AddSubsequence @stateId OUTPUT,'IntakeAlbumin';
EXEC AddSubsequence @stateId OUTPUT,'RemoveDialyzerAndFilter';
EXEC AddSubsequence @stateId OUTPUT,'CheckCitricPh';
EXEC AddSubsequence @stateId OUTPUT,'IntakeSporotal';
EXEC AddSubsequence @stateId OUTPUT,'SporotalCleaning';
EXEC AddSubsequence @stateId OUTPUT,'ROConnectorTest';
EXEC AddSubsequence @stateId OUTPUT,'ROFilling';
EXEC AddSubsequence @stateId OUTPUT,'ROEmpty';
EXEC AddSubsequence @stateId OUTPUT,'FillSystem';
EXEC AddSubsequence @stateId OUTPUT,'RefillSystem';
EXEC AddSubsequence @stateId OUTPUT,'RefillHydraulic';
EXEC AddSubsequence @stateId OUTPUT,'OverflowRinse';
EXEC AddSubsequence @stateId OUTPUT,'WasteSample';
EXEC AddSubsequence @stateId OUTPUT,'ConcTest';
EXEC AddSubsequence @stateId OUTPUT,'PredilutionTest';
EXEC AddSubsequence @stateId OUTPUT,'IntakeSodaCleaner2nd';
EXEC AddSubsequence @stateId OUTPUT,'FillingWithoutPP';
EXEC AddSubsequence @stateId OUTPUT,'CheckForSoda';
EXEC AddSubsequence @stateId OUTPUT,'FanTest';
EXEC AddSubsequence @stateId OUTPUT,'PressureTest';
EXEC AddSubsequence @stateId OUTPUT,'WastePumpATest';
EXEC AddSubsequence @stateId OUTPUT,'WastePumpBTest';
EXEC AddSubsequence @stateId OUTPUT,'ROFlowSensorTest';
EXEC AddSubsequence @stateId OUTPUT,'SupplyPumpTest';
EXEC AddSubsequence @stateId OUTPUT,'ConcentratePumpTest';
EXEC AddSubsequence @stateId OUTPUT,'ROFillingWithPreReservoirCheck';
EXEC AddSubsequence @stateId OUTPUT,'PreCleaning';
EXEC AddSubsequence @stateId OUTPUT,'SodaEmpty';
EXEC AddSubsequence @stateId OUTPUT,'IntermediateCleaning';
EXEC AddSubsequence @stateId OUTPUT,'IntakeSodaCleaner3rd';
EXEC AddSubsequence @stateId OUTPUT,'MainCleaning';
EXEC AddSubsequence @stateId OUTPUT,'MakeAlkalineState';
GO

IF OBJECT_ID('AddSubsequence','P') IS NOT NULL
   DROP PROCEDURE AddSubsequence
GO
