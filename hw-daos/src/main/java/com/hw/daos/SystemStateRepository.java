package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.processcontrol.SystemState;

/**
 * The extension of {@link JpaRepository} for {@link SystemState}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface SystemStateRepository extends JpaRepository<SystemState, Byte[]> {
				

}
