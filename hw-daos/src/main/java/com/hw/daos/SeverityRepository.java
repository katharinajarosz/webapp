package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.errordata.Severity;

/**
 * The extension of {@link JpaRepository} for {@link Severity}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface SeverityRepository extends JpaRepository<Severity, Integer> {
	
	
	
	Severity findById(int severityId);

}
