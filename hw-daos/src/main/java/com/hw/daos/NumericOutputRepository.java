package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.controlsystem.NumericOutput;

/**
 * The extension of {@link JpaRepository} for {@link NumericOutput}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface NumericOutputRepository extends JpaRepository<NumericOutput, Long> {
	
	

}
