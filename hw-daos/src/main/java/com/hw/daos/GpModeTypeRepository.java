package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.controlsystem.types.GpModeType;

/**
 * The extension of {@link JpaRepository} for {@link GpModeType}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface GpModeTypeRepository extends JpaRepository<GpModeType, Integer> {
	
	GpModeType findById(int id);

}
