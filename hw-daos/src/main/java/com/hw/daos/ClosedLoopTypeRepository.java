package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.controlsystem.types.ClosedLoopType;

/**
 * The extension of {@link JpaRepository} for {@link ClosedLoopType}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface ClosedLoopTypeRepository extends JpaRepository<ClosedLoopType, Integer> {
	
	/**
	 * @param id
	 * @return a ClosedLoopType object
	 */
	ClosedLoopType findById(int id);

}
