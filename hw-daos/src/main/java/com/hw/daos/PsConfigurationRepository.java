package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.protectivesystem.PSConfiguration;

/**
 * The extension of {@link JpaRepository} for {@link PSConfiguration}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface PsConfigurationRepository extends JpaRepository<PSConfiguration, Long> {
	
	

}
