package com.hw.daos;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

import com.hw.database.beans.controlsystem.ClosedLoop;

/**
 * The extension of {@link JpaRepository} for {@link ClosedLoop}.
 * 
 * @author kaja
 * @version 1.0
 *
 */
@Transactional
public interface ClosedLoopRepository extends JpaRepository<ClosedLoop, Long> {

}
