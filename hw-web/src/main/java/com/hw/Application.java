package com.hw;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import lombok.extern.slf4j.Slf4j;


/**
 * The Class Application.
 * 
 * Starts the application.
 */
@SpringBootApplication
public class Application {
			   
    	/**
	     * The main method.
	     *
	     * @param args the arguments
	     * @throws Exception the exception
	     */
	    public static void main(String[] args) throws Exception {
	        SpringApplication.run(Application.class, args); 	      
	       
	    }

}